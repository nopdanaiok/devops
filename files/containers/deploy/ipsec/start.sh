docker-compose down || (echo "skip")
apt install -y linux-modules-extra-$(uname -a | awk '{ print $3 } ')
modprobe l2tp_core
modprobe l2tp_netlink
modprobe l2tp_eth
modprobe l2tp_ppp
#modprobe pppol2tp
docker-compose up -d
