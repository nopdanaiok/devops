#!/usr/bin/env python3.10
import time
from selenium import webdriver
import os
import dotenv
import sys
import subprocess
import requests
from requests.auth import HTTPDigestAuth
import time
import datetime
from datetime import datetime, timedelta
from time import gmtime, strftime

dotenv.load_dotenv()


TELEGRAM_BOT_TOKEN = os.getenv("TELEGRAM_BOT_TOKEN","1801387352:AAEtqO2MRDXbqW6iB225T5HKJtmjBYNvh72")
TELEGRAM_CHANNEL_ID = os.getenv("TELEGRAM_CHANNEL_ID","-1001802164071")
#print(TELEGRAM_BOT_TOKEN)
#print(TELEGRAM_CHANNEL_ID)


def telegram_send(t_bot_token, t_group_id, t_text):
    url = 'https://api.telegram.org/bot' + t_bot_token + '/sendMessage'
    payload = {'chat_id': t_group_id,
               'disable_web_page_preview': 1,
               'disable_notification': 0,
               'text': t_text}
    print('Send to telegram')
    print("url= %(r_url)s payload %(r_payload)s \
        " % {"r_url": url, "r_payload": payload})
    r = requests.post(url, data=payload)


for attempt in range(5):
    try:
        options = webdriver.ChromeOptions()
        options.add_argument("--host-resolver-rules=MAP *google* 127.0.0.1")
        browser = webdriver.Remote(
            options=options,
            command_executor="http://%s:%s" % (os.getenv("SELENIUM_HOST", "127.0.0.1"), os.getenv("SELENIUM_PORT", "4445")),
        )
        browser.close()
        print("Healtchcheck finished good at " + str(datetime.now().strftime("%Y-%d-%H %H:%M:%S")), file=sys.stdout)
        time.sleep(2)
        exit(0)
    except Exception as error:
        print("Healtchcheck bad for attempts " + str(attempt), file = sys.stdout)
        print(error)
        time.sleep(2)
print("Healtchcheck finished bad. Try restart containder selenoid-chrome-stack-selenoid-1-1", file = sys.stdout)

try:
    p1 = subprocess.Popen("/usr/local/bin/docker-compose kill --remove-orphans", stdout=subprocess.PIPE, shell=True)
    print(p1.communicate())
except Exception:
    pass

try:
    p2 = subprocess.Popen("/usr/local/bin/docker-compose up -d", stdout=subprocess.PIPE, shell=True)
    print(p2.communicate())
    t_text = "selenoid-chrome-stack-selenoid-1-1 restarted"
    telegram_send(TELEGRAM_BOT_TOKEN, TELEGRAM_CHANNEL_ID, t_text)
except Exception as error:
    print("Error during exec /usr/local/bin/docker-compose up -d")

#exit(1)
