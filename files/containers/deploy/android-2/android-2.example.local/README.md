# Android-1 emulator

### Features

 - android emulator autostart
 - vnc server(port 5900)
 - work out of box

### Req

- docker
- docker-compose
- kvm enabled

### Clone

Clone from open repo https://gitlab.com/devops-f/devops.git

```sh
mkdir ${HOME}/git
cd ${HOME}/git
git clone -b main https://gitlab.com/devops-f/devops.git
cd devops/files/containers/deploy/android-1/android.example.local/
```

### Configure

Change docker-compose.yml(if neccesary)

**Enable openvpn autostart**

```sh
    OPENVPN_ENABLED: "1" # if 0 vpn disabled
    OPENVPN_FILES_DIR: "/data/openvpn/current"
    OPENVPN_CUSTOM_ROUTES: "10.50.0.0/24 172.16.0.0/12 192.168.0.0/16"
```
put file openvpn.opvpn in ./data/openvpn/current.

Add lines to  file ./data/openvpn/current/openvpn.opvpn

```sh
# server vpn interface is up
up "./client-connect.sh"

# server vpn interface is going down
down "./client-disconnect.sh"
```

it will exec  script's during start and stop (configure static routes) from OPENVPN_CUSTOM_ROUTES variable


**ANDROID_ADV_NAME - android emulator app**

Allowed devices Pixel_2_API_29(microg), Pixel_2(sdk gphone)


**Custom android apk's files install during start**

If you want install custom apk file during start put it in dir. 

```sh
./data/android/apk/${ANDROID_ADV_NAME}
```

!!Warning: default apk's not been installed if existed line in volume mount block ```#./data/android/apk:/android/apk/``` in docker-compose.yml will uncomment!!

Default apk's file open you can get from repo [gitlab repo](https://gitlab.com/cdn72/extra-files/-/tree/main/files/apks?ref_type=heads)

### Start

```sh
docker-compose pull && docker-compose up -d backend
```



After container init you can connect via vnc and application ports

```sh

- 5555 adb

- 5900 vnc (via remmina or other app)

- 27042 frida server
```

If you will use Pixel_2_API_29(microg) you need  after start and init container  configure "Microg app".

- Google device registration - On
- Google Messaging - On
- Google SafetyNet - ON

![Pixel_2_API_29_microg_app.png](https://gitlab.com/cdn72/media/-/raw/main/files/containers/deploy/android-1/android.example.local/Pixel_2_API_29_microg_app.png "Pixel_2_API_29_microg_app.png")

After restart container all setting will be lost.

### Logs

```sh
docker-compose logs -f --tail 100
```

### Watcher container(in dev env you can skip it)

Watcher container monitor backend container and restart services in backend container if it fail. 

### Stop

```sh
docker-compose down
```