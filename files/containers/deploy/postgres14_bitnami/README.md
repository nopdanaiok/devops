# Deploy postgresql 14 with timescaledb extension

## Clone repo and stack docker with compose


```sh
mkdir -p /git && git clone https://gitlab.com/devops-f/devops.git /git/devops
cd /git/devops/files/containers/deploy/postgres14_bitnami &&  docker-compose up -d
```

## Create database in docker and add extension's

Login in container

```sh
docker exec -ti postgres14_bitnami-postgres-14-1-bitnami-1 bash
```

create database user and database for logs via script;

```sh
bash /pgscripts/02-create-db.sh dbuser dbpassword dbname
```


Create  extension's in database 

```sh
postgresroot
```

```sql
\c dbname
SET timezone = 'UTC';
CREATE EXTENSION timescaledb;
CREATE EXTENSION timescaledb_toolkit;
SELECT default_version, installed_version FROM pg_available_extensions where name = 'timescaledb';
SELECT default_version, installed_version FROM pg_available_extensions where name = 'timescaledb_toolkit';
```

## Connect to postgres container from host via psql

```sh
export YOUR_HOST_IP="127.0.0.1"
PGPASSWORD=dbpassword psql --host ${YOUR_HOST_IP} --port 5432  -U dbuser -d dbname
```

## Create table log_records in db dbname, create hypertable and retention policy


```sql
CREATE TABLE log_records (
                            time   TIMESTAMP NOT NULL,
                            tag    CHAR(128) NOT NULL,
                            record JSONB     NOT NULL
);
SELECT create_hypertable('log_records', 'time',chunk_time_interval => INTERVAL '1 day',if_not_exists => TRUE);
SELECT add_retention_policy('log_records', INTERVAL '15 days',if_not_exists => TRUE);





```


## Configure fluentd for send data to database

```xml
<match **>
  @type timescaledb
  db_conn_string "host=127.0.0.1 user=dbuser password=dbpassword dbname=dbname"
</match>
```



## Mans

- [timescaledb](https://docs.timescale.com/use-timescale/latest/hypertables/)

- [/fluent-plugin-timescaledb](https://github.com/1500cloud/fluent-plugin-timescaledb)

