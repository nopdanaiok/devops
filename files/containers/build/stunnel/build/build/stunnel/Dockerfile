ARG DOCKER_IMAGE_BASE
FROM ${DOCKER_IMAGE_BASE}

# See tinyproxy.conf for better explanation of these values.
# Insert any value (preferably "yes") to disable the Via-header:
ENV DISABLE_VIA_HEADER ""
# Set this to e.g. tinyproxy.stats to enable stats-page on that address:
ENV STAT_HOST ""
ENV MAX_CLIENTS ""
ENV MIN_SPARE_SERVERS ""
ENV MAX_SPARE_SERVERS ""
# A space separated list:
ENV ALLOWED_NETWORKS ""

# Use a custom UID/GID instead of the default system UID which has a greater possibility
# for collisions with the host and other containers.
ENV TINYPROXY_UID 57981
ENV TINYPROXY_GID 57981

RUN apk add --no-cache \
      tinyproxy \
      bash

RUN mv /etc/tinyproxy/tinyproxy.conf /etc/tinyproxy/tinyproxy.default.conf && \
    chown -R ${TINYPROXY_UID}:${TINYPROXY_GID} /etc/tinyproxy /var/log/tinyproxy

EXPOSE 8888

# Tinyproxy seems to be OK for getting privileges dropped beforehand

ADD files/docker-entrypoint.sh  /docker-entrypoint.sh
RUN chmod 775 /docker-entrypoint.sh && tinyproxy -v && chown -R ${TINYPROXY_UID}:${TINYPROXY_GID} /docker-entrypoint.sh
USER ${TINYPROXY_UID}:${TINYPROXY_GID}
ENTRYPOINT ["/docker-entrypoint.sh"]