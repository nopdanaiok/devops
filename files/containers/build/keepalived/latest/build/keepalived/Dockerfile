ARG BASE_IMAGE_TAG
FROM ${BASE_IMAGE_TAG}
ENV BASE_IMAGE_TAG "${BASE_IMAGE_TAG}"
ARG APP_VERSION
ARG APP_NAME
ENV APP_NAME "${APP_NAME}"
ENV APP_VERSION "${APP_VERSION}"

LABEL architecture="x86_64"                       \
      build-date="$BUILD_DATE"                    \
      license="MIT"                               \
      name="registry.gitlab.com/linux2be.com/devops/keepalived"  \
      summary="Alpine based keepalived container" \
      version="${APP_VERSION}"                     \
      vcs-ref="$VCS_REF"                          \
      vcs-type="git"                              \
      vcs-url="https://gitlab.com/linux2be.com/devops.git"

RUN apk add --no-cache \
    bash       \
    curl       \
    ipvsadm    \
    iproute2   \
    keepalived \
    nano
RUN mkdir -p /etc/keepalived
COPY files/docker-entrypoint.sh /docker-entrypoint.sh
COPY files/scripts/chk_kube_apiserver.sh /usr/lib/keepalived/scripts/chk_kube_apiserver.sh
RUN chmod +x /docker-entrypoint.sh /usr/lib/keepalived/scripts/chk_kube_apiserver.sh

ARG BUILD_VERSION
ENV BUILD_VERSION "${BUILD_VERSION}"

SHELL ["/bin/bash", "-c"]

WORKDIR /usr/lib/keepalived
ENTRYPOINT ["/docker-entrypoint.sh"]

STOPSIGNAL SIGTERM