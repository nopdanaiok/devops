FROM docker.io/bitnami/minideb:bullseye
ARG EXTRA_LOCALES
ARG TARGETARCH
ARG WITH_ALL_LOCALES="no"

ENV HOME="/" \
    OS_ARCH="${TARGETARCH:-amd64}" \
    OS_FLAVOUR="debian-11" \
    OS_NAME="linux"

ARG APP_VERSION
ENV APP_VERSION "${APP_VERSION}"
ARG APP_RELEASE
ENV APP_RELEASE "${APP_RELEASE}"

ENV DOCKER_IMAGE_RUN_USER "app-user"
ENV DEBIAN_FRONTEND=noninteractive

COPY prebuildfs /
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN install_packages ca-certificates curl libbsd0 libbz2-1.0 libedit2 libffi7 libgcc-s1 libgmp10 libgnutls30 libhogweed6 libicu67 libidn2-0 libldap-2.4-2 liblz4-1 liblzma5 libmd0 libncurses6 libnettle8 libp11-kit0 libpcre3 libreadline8 libsasl2-2 libsqlite3-0 libssl1.1 libstdc++6 libtasn1-6 libtinfo6 libunistring2 libuuid1 libxml2 libxslt1.1 libzstd1 locales procps zlib1g
RUN mkdir -p /tmp/bitnami/pkg/cache/ && cd /tmp/bitnami/pkg/cache/ && \
    COMPONENTS=( \
      "gosu-1.14.0-155-linux-${OS_ARCH}-debian-11" \
      "postgresql-15.4.0-1-linux-${OS_ARCH}-debian-11" \
    ) && \
    for COMPONENT in "${COMPONENTS[@]}"; do \
      if [ ! -f "${COMPONENT}.tar.gz" ]; then \
        curl -SsLf "https://downloads.bitnami.com/files/stacksmith/${COMPONENT}.tar.gz" -O ; \
        curl -SsLf "https://downloads.bitnami.com/files/stacksmith/${COMPONENT}.tar.gz.sha256" -O ; \
      fi && \
      sha256sum -c "${COMPONENT}.tar.gz.sha256" && \
      tar -zxf "${COMPONENT}.tar.gz" -C /opt/bitnami --strip-components=2 --no-same-owner --wildcards '*/files' && \
      rm -rf "${COMPONENT}".tar.gz{,.sha256} ; \
    done

RUN chmod g+rwX /opt/bitnami
RUN localedef -c -f UTF-8 -i en_US en_US.UTF-8
RUN update-locale LANG=C.UTF-8 LC_MESSAGES=POSIX && \
    DEBIAN_FRONTEND=noninteractive dpkg-reconfigure locales
RUN echo 'en_GB.UTF-8 UTF-8' >> /etc/locale.gen && locale-gen
RUN echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen && locale-gen

COPY rootfs /

RUN /opt/bitnami/scripts/postgresql/postunpack.sh
RUN /opt/bitnami/scripts/locales/add-extra-locales.sh

ENV BITNAMI_APP_NAME="postgresql" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    NSS_WRAPPER_LIB="/opt/bitnami/common/lib/libnss_wrapper.so" \
    PATH="/opt/bitnami/common/bin:/opt/bitnami/postgresql/bin:$PATH"

ENV DOCKER_IMAGE_RUN_USER "app-user"
RUN groupadd -g 1001 ${DOCKER_IMAGE_RUN_USER:-app-user} \
    && useradd -u 1001 -r -g 1001 -d "/home/${DOCKER_IMAGE_RUN_USER:-app-user}" -c "Default Application User" ${DOCKER_IMAGE_RUN_USER:-app-user} \
    && mkdir -p /home/${DOCKER_IMAGE_RUN_USER:-app-user} \
    && usermod -aG www-data ${DOCKER_IMAGE_RUN_USER:-app-user} \
    && chown -R  ${DOCKER_IMAGE_RUN_USER:-app-user}:${DOCKER_IMAGE_RUN_USER:-app-user} /home/${DOCKER_IMAGE_RUN_USER:-app-user} \
    && chown -R 1001:1001 /opt/bitnami /bitnami

ARG DOCKER_BUILD_DATE
ARG PROVISIONING_VERSION
ENV PROVISIONING_VERSION "${PROVISIONING_VERSION}"
ARG EXTRA_FILES_VERSION
ENV EXTRA_FILES_VERSION "${EXTRA_FILES_VERSION}"
ARG APP_VENDOR_MAIN
ENV APP_VENDOR_MAIN "${APP_VENDOR_MAIN}"
ENV PG_EXTENSION_SKIPPED=""
ARG APP_VERSION_PG_REPACK
ENV APP_VERSION_PG_REPACK "${APP_VERSION_PG_REPACK}"
ARG APP_VERSION_PG_HINT_PLAN
ENV APP_VERSION_PG_HINT_PLAN "${APP_VERSION_PG_HINT_PLAN}"
ARG APP_VERSION_PG_STAT_MONITOR
ENV APP_VERSION_PG_STAT_MONITOR "${APP_VERSION_PG_STAT_MONITOR}"
ENV GIT_REPO_TIMESCALEDB "https://github.com/timescale/timescaledb.git"
ARG APP_VERSION_PG_TIMESCALEDB
ENV APP_VERSION_PG_TIMESCALEDB "${APP_VERSION_PG_TIMESCALEDB}"
ARG APP_VERSION_PG_TIMESCALEDB_EXISTS_COMPILED
ARG APP_VERSION_PG_TIMESCALEDB_TOOLKIT
ENV APP_VERSION_PG_TIMESCALEDB_TOOLKIT "${APP_VERSION_PG_TIMESCALEDB_TOOLKIT}"
ARG APP_VERSION_PG_TIMESCALEDB_TOOLKIT_EXISTS_COMPILED
ENV APP_VERSION_PG_IP4R "2.4"
ENV APP_VERSION_PG_IP4R_EXISTS_COMPILED "true"
COPY rootfs /
RUN echo "DOCKER_BUILD_DATE=${DOCKER_BUILD_DATE}" \
    && cd /tmp/ \
    && curl -O -L "https://gitlab.com/cdn72/provisioning/-/raw/${PROVISIONING_VERSION:-master}/files/libs.d/dockers/postgres/bitnami_docker_after_build.sh" \
    && echo "Run script /tmp/bitnami_docker_after_build.sh" \
    && bash /tmp/bitnami_docker_after_build.sh && chown -R 1001:1001 /opt/bitnami /bitnami

RUN /bin/bash -C "/usr/sbin/initbash_profile.sh"
VOLUME [ "/bitnami/postgresql", "/docker-entrypoint-initdb.d", "/docker-entrypoint-preinitdb.d" ]

EXPOSE 5432

USER 1001
ENTRYPOINT [ "/opt/bitnami/scripts/postgresql/entrypoint.sh" ]
CMD [ "/opt/bitnami/scripts/postgresql/run.sh" ]
