#!/usr/bin/env bash
export COMPILE_DIR="/usr/src"
export NB_PROC=12
export APP_VERSION=1.22.0
export  VERSION_NGINX=nginx-$APP_VERSION
export  SOURCE_LIBRESSL=http://ftp.openbsd.org/pub/OpenBSD/LibreSSL/
export  VERSION_PCRE=8.42
export  SOURCE_NGINX=http://nginx.org/download/
export  VERSION_NGINX_DEVEL_KIT=0.3.1
export  VERSION_LUA_NGINX_MODULE=0.10.21
export  VERSION_LUA_JIT=2.1.0-beta3
export  LUAJIT_LIB=/usr/local/LuaJIT/lib
export  LUAJIT_INC=/usr/local/LuaJIT/include/luajit-2.1
export BUIL_ARG_1="http2"
if [ "${BUIL_ARG_1}" == "http1" ];then additional_params="--with-http_ssl_module"; else additional_params="--with-http_ssl_module --with-http_v2_module ";fi


step_1() {
echo "build" \
     && echo "BUIL_ARG_1=${BUIL_ARG_1}" \
     && echo "additional_params=$additional_params" \
     && apk add --no-cache \
        openldap-dev \
        pcre \
        zlib \
        bash \
        curl \
        libressl \
        tzdata \
        ca-certificates \
        tini \
        shadow \
        brotli-libs \
        inotify-tools \
    && deluser xfs || (echo "skip !!" ) \
    && delgroup www-data  || (echo "skip !!" ) \
    && addgroup -g 33 -S www-data || (echo "skip !!" ) \
    && adduser -u 33 -D -S -G www-data www-data || (echo "skip !!" ) \
    && addgroup -g 101 -S nginx || (echo "skip !!" ) \
    && adduser -u 101 -D -S -G nginx nginx || (echo "skip !!" ) \
    && /usr/sbin/usermod -aG www-data nginx || (echo "skip !!" ) \
    && apk add --no-cache --virtual .build-deps \
        build-base \
        autoconf \
        automake \
        bind-tools \
        binutils \
        build-base \
        ca-certificates \
        cmake \
        file \
        gcc \
        gd-dev \
        geoip-dev \
        git \
        gnupg \
        libc-dev \
        libstdc++ \
        readline \
        libtool \
        libxslt-dev \
        linux-headers \
        make \
        patch \
        pcre \
        pcre-dev \
        perl-dev \
        su-exec \
        tar \
        zlib \
        go \
        zlib-dev \
        libgcc \
        ca-certificates \
 && mkdir -p ${COMPILE_DIR}  \
 && cd $COMPILE_DIR && rm -rfv cd $COMPILE_DIR/* \
 && curl -L http://nginx.org/download/${VERSION_NGINX}.tar.gz -o ${VERSION_NGINX}.tar.gz \
 && curl -L https://gitlab.com/cdn72/extra-files/-/raw/main/files/pcre-${VERSION_PCRE}.tar.gz -o $COMPILE_DIR/pcre-${VERSION_PCRE}.tar.gz \
 && curl -L https://github.com/simplresty/ngx_devel_kit/archive/v${VERSION_NGINX_DEVEL_KIT}.tar.gz -o v${VERSION_NGINX_DEVEL_KIT}.tar.gz \
 && curl -L https://github.com/openresty/lua-nginx-module/archive/v${VERSION_LUA_NGINX_MODULE}.tar.gz -o v${VERSION_LUA_NGINX_MODULE}.tar.gz \
 && curl -L http://luajit.org/download/LuaJIT-${VERSION_LUA_JIT}.tar.gz -o LuaJIT-${VERSION_LUA_JIT}.tar.gz \
 && curl -L https://people.freebsd.org/~osa/ngx_http_redis-0.3.9.tar.gz -o ngx_http_redis-0.3.9.tar.gz \
 && git clone https://github.com/vozlt/nginx-module-vts.git nginx-module-vts \
 && git clone https://github.com/yaoweibin/ngx_http_substitutions_filter_module.git ngx_http_substitutions_filter_module \
 && git clone https://github.com/kvspb/nginx-auth-ldap.git nginx-auth-ldap \
 && git clone https://github.com/openresty/srcache-nginx-module.git srcache-nginx-module \
 && git clone https://github.com/openresty/echo-nginx-module.git echo-nginx-module \
 && git clone https://github.com/openresty/redis2-nginx-module.git redis2-nginx-module \
 && git clone https://github.com/openresty/rds-json-nginx-module.git rds-json-nginx-module \
 && git clone https://github.com/calio/form-input-nginx-module form-input-nginx-module \
 && git clone https://github.com/yaoweibin/nginx_upstream_check_module.git nginx_http_upstream_check_module \
 && git clone https://github.com/Lax/traffic-accounting-nginx-module.git traffic-accounting-nginx-module \
 && git clone https://github.com/google/ngx_brotli.git ngx_brotli && cd $COMPILE_DIR/ngx_brotli && git submodule update --init \
    && (git clone --depth=1 https://boringssl.googlesource.com/boringssl ${COMPILE_DIR}/boringssl \
        && sed -i 's@out \([>=]\) TLS1_2_VERSION@out \1 TLS1_3_VERSION@' ${COMPILE_DIR}/boringssl/ssl/ssl_lib.cc \
        && sed -i 's@ssl->version[ ]*=[ ]*TLS1_2_VERSION@ssl->version = TLS1_3_VERSION@' ${COMPILE_DIR}/boringssl/ssl/s3_lib.cc \
        && sed -i 's@(SSL3_VERSION, TLS1_2_VERSION@(SSL3_VERSION, TLS1_3_VERSION@' ${COMPILE_DIR}/boringssl/ssl/ssl_test.cc \
        && sed -i 's@\$shaext[ ]*=[ ]*0;@\$shaext = 1;@' ${COMPILE_DIR}/boringssl/crypto/*/asm/*.pl \
        && sed -i 's@\$avx[ ]*=[ ]*[0|1];@\$avx = 2;@' ${COMPILE_DIR}/boringssl/crypto/*/asm/*.pl \
        && sed -i 's@\$addx[ ]*=[ ]*0;@\$addx = 1;@' ${COMPILE_DIR}/boringssl/crypto/*/asm/*.pl \
        && mkdir -p ${COMPILE_DIR}/boringssl/build ${COMPILE_DIR}/boringssl/.openssl/lib ${COMPILE_DIR}/boringssl/.openssl/include \
        && ln -sf ${COMPILE_DIR}/boringssl/include/openssl ${COMPILE_DIR}/boringssl/.openssl/include/openssl \
        && touch ${COMPILE_DIR}/boringssl/.openssl/include/openssl/ssl.h \
        && cmake -B${COMPILE_DIR}/boringssl/build -H${COMPILE_DIR}/boringssl -DCMAKE_BUILD_TYPE=RelWithDebInfo \
        && make -C${COMPILE_DIR}/boringssl/build -j$(getconf _NPROCESSORS_ONLN) \
        && cp ${COMPILE_DIR}/boringssl/build/crypto/libcrypto.a ${COMPILE_DIR}/boringssl/build/ssl/libssl.a ${COMPILE_DIR}/boringssl/.openssl/lib/) \
 && cd $COMPILE_DIR \
 && tar xzf $VERSION_NGINX.tar.gz \
 && tar xzf ngx_http_redis-0.3.9.tar.gz \
 && tar xzf pcre-${VERSION_PCRE}.tar.gz \
 && tar xzf v${VERSION_NGINX_DEVEL_KIT}.tar.gz \
 && tar xzf v${VERSION_LUA_NGINX_MODULE}.tar.gz \
 && tar xzf LuaJIT-${VERSION_LUA_JIT}.tar.gz
#echo "exit0" && exit 0
}


step_2() {
 #&& ln -sf luajit-${VERSION_LUA_JIT} /usr/local/LuaJIT/bin/luajit \
 cd $COMPILE_DIR/LuaJIT-${VERSION_LUA_JIT} && make PREFIX=/usr/local/luajit && make install PREFIX=/usr/local/LuaJIT \
 && cd $COMPILE_DIR/$VERSION_NGINX \
 && if [[ ! -f "$COMPILE_DIR/$VERSION_NGINX/dynamic_tls_records.patch" ]]; then curl -fSL https://raw.githubusercontent.com/nginx-modules/ngx_http_tls_dyn_size/0.5/nginx__dynamic_tls_records_1.17.7%2B.patch -o dynamic_tls_records.patch  && patch -p1 < dynamic_tls_records.patch;fi \
 && cd $COMPILE_DIR/$VERSION_NGINX && ./configure ${additional_params} \
--with-cc-opt="-g -O2 -fPIE -fstack-protector-all -D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security -I ${COMPILE_DIR}/boringssl/.openssl/include/" \
--with-ld-opt="-Wl,-Bsymbolic-functions -Wl,-z,relro -L ${COMPILE_DIR}/boringssl/.openssl/lib/,-Wl,-rpath,$LUAJIT_LIB" \
--sbin-path=/usr/sbin/nginx \
--conf-path=/etc/nginx/nginx.conf \
--error-log-path=/var/log/nginx/error.log \
--http-log-path=/var/log/nginx/access.log \
--lock-path=/var/lock/nginx.lock \
--pid-path=/run/nginx.pid \
--with-http_slice_module \
--with-file-aio \
--with-http_sub_module \
--with-http_gzip_static_module \
--with-http_stub_status_module \
--with-http_image_filter_module \
--with-threads \
--with-mail \
--with-http_dav_module \
--with-mail_ssl_module \
--with-stream \
--with-stream_realip_module \
--with-stream_ssl_module \
--with-stream_ssl_preread_module \
--http-client-body-temp-path=/var/lib/nginx/body \
--http-fastcgi-temp-path=/var/lib/nginx/fastcgi \
--http-proxy-temp-path=/var/lib/nginx/proxy \
--http-scgi-temp-path=/var/lib/nginx/scgi \
--http-uwsgi-temp-path=/var/lib/nginx/uwsgi \
--with-debug \
--with-pcre-jit \
--with-http_realip_module \
--with-http_gunzip_module \
--with-http_auth_request_module \
--with-http_addition_module \
--with-http_gzip_static_module \
--with-openssl="${COMPILE_DIR}/boringssl/.openssl/include" \
--add-module=$COMPILE_DIR/ngx_devel_kit-${VERSION_NGINX_DEVEL_KIT} \
--add-module=$COMPILE_DIR/lua-nginx-module-${VERSION_LUA_NGINX_MODULE} \
--add-module=$COMPILE_DIR/ngx_http_substitutions_filter_module \
--add-module=$COMPILE_DIR/nginx-auth-ldap \
--add-module=$COMPILE_DIR/nginx-module-vts \
--add-module=$COMPILE_DIR/srcache-nginx-module \
--add-module=$COMPILE_DIR/echo-nginx-module \
--add-module=$COMPILE_DIR/redis2-nginx-module \
--add-module=$COMPILE_DIR/ngx_http_redis-0.3.9 \
--add-module=$COMPILE_DIR/ngx_brotli \
--add-module=$COMPILE_DIR/rds-json-nginx-module \
--add-module=$COMPILE_DIR/form-input-nginx-module \
&& cd $COMPILE_DIR/$VERSION_NGINX \
&& make -j 12 \
&& make install
}
step_1
step_2