ARG DOCKER_IMAGE_BASE
FROM ${DOCKER_IMAGE_BASE}
ARG APP_NAME
ARG APP_VERSION
ARG APP_RUN_UID
ARG APP_RUN_GID
ARG APP_DIR_CURRENT
ENV APP_NAME="${APP_NAME}"
ENV APP_VERSION="${APP_VERSION}"
ENV APP_DIR_CURRENT="${APP_DIR_CURRENT}"
WORKDIR /tmp
RUN apt update  \
    && apt install curl -y \
    && groupadd -g "${APP_RUN_GID:-10001}" "${DOCKER_IMAGE_RUN_USER:-exporter_blackbox}" \
    && useradd --no-log-init -u "${APP_RUN_UID:-10001}" -r -g "${APP_RUN_GID:-10001}" -d "${APP_DIR_CURRENT}" -c "Default Application User ${APP_NAME}" "${DOCKER_IMAGE_RUN_USER:-exporter_blackbox}" \
    && mkdir -p "${APP_DIR_CURRENT}" \
    && chown -R  "${DOCKER_IMAGE_RUN_USER:-exporter_blackbox}":"${DOCKER_IMAGE_RUN_USER:-exporter_blackbox}" ${APP_DIR_CURRENT} \
    && curl -LO "https://github.com/prometheus/blackbox_exporter/releases/download/v${APP_VERSION:-0.24.0}/blackbox_exporter-${APP_VERSION:-0.24.0}.linux-amd64.tar.gz" \
    && tar -xvf "blackbox_exporter-${APP_VERSION:-0.24.0}.linux-amd64.tar.gz" \
    && mv "blackbox_exporter-${APP_VERSION:-0.24.0}.linux-amd64/blackbox_exporter" /usr/local/bin/ \
    && chmod +x /usr/local/bin/blackbox_exporter \
&& rm -rf \
    /var/lib/apt/lists/* \
    /tmp/* \
    /var/tmp/* \
    /usr/share/man \
    /usr/share/doc \
    /usr/share/doc-base
COPY files/example.yml ${APP_DIR_CURRENT}/blackbox.yml
RUN chown -R  "${DOCKER_IMAGE_RUN_USER:-exporter_blackbox}":"${DOCKER_IMAGE_RUN_USER:-exporter_blackbox}" ${APP_DIR_CURRENT} \
    && /usr/local/bin/blackbox_exporter --help

WORKDIR ${APP_DIR_CURRENT}
ENTRYPOINT ["blackbox_exporter"]
CMD ["--config.file=blackbox.yml"]
