PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/open-xchange/sbin/

ox_listuser(){
/opt/open-xchange/sbin/listuser \
--contextid="${ox_context_id}" --adminuser="${ox_oxadminuser_name}" --adminpass="${ox_oxadminuser_password}" 

}


ox_deleteuser(){
	
	arg1="$@"
	if [[ $arg1 == "" ]];then
		echo "Error.send arg1 user id for delete"
	else
		ox_user_id=$(/opt/open-xchange/sbin/listuser \
--contextid="${ox_context_id}" --adminuser="${ox_oxadminuser_name}" --adminpass="${ox_oxadminuser_password}" \
| awk '$1 ~ /'^"$arg1"'/' | awk '{print $1}')
		ox_user_login=$(/opt/open-xchange/sbin/listuser \
--contextid="${ox_context_id}" --adminuser="${ox_oxadminuser_name}" --adminpass="${ox_oxadminuser_password}" \
| awk '$1 ~ /'^"$arg1"'/' | awk '{print $2}')
		echo "Try delete user with id $arg1. Founded ox_user_id=$ox_user_id ox_user_login=$ox_user_login"
		if [[ "${ox_user_id}"  == "$arg1" ]];then
			echo "delete user with id $ox_user_id and ox_user_login=$ox_user_login "
			/opt/open-xchange/sbin/deleteuser \
			--contextid="${ox_context_id}" --adminuser="${ox_oxadminuser_name}" --adminpass="${ox_oxadminuser_password}" \
			--userid=$arg1
		else
			echo "Error.os_user with id $arg1 not exist"
			
		fi
fi
}