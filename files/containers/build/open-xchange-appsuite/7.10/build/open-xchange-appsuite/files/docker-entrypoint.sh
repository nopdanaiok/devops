#!/usr/bin/env bash
arg_1="${1}"

if [ -z "${ox_app_shared_dir_etc}" ];then ox_app_shared_dir_etc="/persist/open-xchange/etc";fi

env_show(){
  export -p | grep ox_
}

app_prepare(){
  echo "exec funct ${FUNCNAME}"
  
  env_show

  if [[ -f "${ox_app_shared_dir_etc}/app_firs_start_done" ]];then
    echo "Trigger file app_firs_start_done exist in ${ox_app_shared_dir_etc}";
    #echo "Copy data ${ox_app_shared_dir_etc}/ /opt/open-xchange/etc/;"
    echo "Make symlink ${ox_app_shared_dir_etc} /opt/open-xchange/etc"
    #rsync -avP  ${ox_app_shared_dir_etc}/ /opt/open-xchange/etc/;
    if [[ -d "/opt/open-xchange/etc" && -f "${ox_app_shared_dir_etc}/app_firs_start_done" ]];then
      echo "/opt/open-xchange/etc is dir. File ${ox_app_shared_dir_etc}/app_firs_start_done exist";
      echo "/opt/open-xchange/etc /opt/open-xchange/etc.origin";
      mv /opt/open-xchange/etc /opt/open-xchange/etc.origin;
    fi
    ln -sfn ${ox_app_shared_dir_etc} /opt/open-xchange/etc;
    ls -l /opt/open-xchange/etc;
    sleep 5;
    app_configs_fix;
    fix_rights
  fi

}

app_configs_fix(){
  ##settings dcs
  ox_max_upload_size=$(echo "${ox_max_upload_size:-3145728000}" )

  sed -i -e "s/^db.host=.*/db.host=${ox_mysql_db_host:-mysql}/" /etc/documents-collaboration/dcs.properties
  sed -i -e "s/^db.port=.*/db.port=${ox_mysql_db_port:-3306}/" /etc/documents-collaboration/dcs.properties
  sed -i -e "s/^db.username=.*/db.username=${ox_mysql_db_user:-root}/" /etc/documents-collaboration/dcs.properties
  sed -i -e "s/^db.password=.*/db.password=${ox_mysql_db_password:-yourmysqlrootpassword}/" /etc/documents-collaboration/dcs.properties
  sed -i -e "s/^db.schema=.*/db.schema=${dcsdb_db_name:-dcsdb}/" /etc/documents-collaboration/dcs.properties
  grep -q '^jmx.enabled=.*' /etc/documents-collaboration/dcs.properties && sed -i -e "s/^jmx.enabled=.*/jmx.enabled=true/" \
              /etc/documents-collaboration/dcs.properties || echo "jmx.enabled=true" >> /etc/documents-collaboration/dcs.properties
  ##settings documents
  sed -i \
    -e 's/# com.openexchange.capability.presentation=.*/com.openexchange.capability.presentation=true/' \
    -e 's/# com.openexchange.capability.text/com.openexchange.capability.text/1' \
    -e 's/# com.openexchange.capability.spreadsheet/com.openexchange.capability.spreadsheet/1' \
    /opt/open-xchange/etc/documents.properties

  ##settings configdb
  sed -i -e "s/com.openexchange.database.replicationMonitor=.*/com.openexchange.database.replicationMonitor=${ox_setting_database_replicationmonitor:-false}/" /opt/open-xchange/etc/configdb.properties

  ##settings documents-collaboration-client    
  sed -i -e "s/^com.openexchange.dcs.client.database.connectionURL=.*/com.openexchange.dcs.client.database.connectionURL=jdbc:mysql:\/\/${ox_mysql_db_host}:${ox_mysql_db_port}\/dcsdb/"  /opt/open-xchange/etc/documents-collaboration-client.properties
  sed -i -e "s/^com.openexchange.dcs.client.database.userName=.*/com.openexchange.dcs.client.database.userName=${ox_mysql_db_user}/" /opt/open-xchange/etc/documents-collaboration-client.properties
  sed -i -e "s/^com.openexchange.dcs.client.database.password=.*/com.openexchange.dcs.client.database.password=${ox_mysql_db_password}/" /opt/open-xchange/etc/documents-collaboration-client.properties
  sed -i -e "s/^com.openexchange.dcs.client.database.jdbc.useSSL=.*/com.openexchange.dcs.client.database.jdbc.useSSL=${ox_dcs_user_ssl:false}/" /opt/open-xchange/etc/documents-collaboration-client.properties

  ##settings hazelcast
  sed -i -e "s/^com.openexchange.hazelcast.group.password=.*/com.openexchange.hazelcast.group.password=${ox_hazelcast_group_pass}/" /opt/open-xchange/etc/hazelcast.properties

  ##settings server
  sed -i -e "s/^com.openexchange.IPCheck=.*/com.openexchange.IPCheck=${ox_ipcheck}/" /opt/open-xchange/etc/server.properties
  sed -i -e "s/^com.openexchange.connector.networkListenerHost=.*/com.openexchange.connector.networkListenerHost=${ox_networklistenerhost}/" /opt/open-xchange/etc/server.properties
  sed -i -e "s/^com.openexchange.cookie.hash.salt=.*/com.openexchange.cookie.hash.salt=${ox_cookie_hash_salt:-OIaNGBBQvR/wMrGFi00mOg}/" /opt/open-xchange/etc/server.properties
  sed -i -e "s/^com.openexchange.rest.services.basic-auth.login=.*/com.openexchange.rest.services.basic-auth.login=${ox_rest_services_basic_auth_login:-rest-basic-auth}/" /opt/open-xchange/etc/server.properties
  sed -i -e "s/^com.openexchange.rest.services.basic-auth.password=.*/com.openexchange.rest.services.basic-auth.password==${ox_rest_services_basic_auth_password:-A0bPJeVXj5d++GcB4JqqTQ}/" /opt/open-xchange/etc/server.properties
  sed -i -e "s/MAX_UPLOAD_SIZE=.*/MAX_UPLOAD_SIZE=${ox_max_upload_size}/" /opt/open-xchange/etc/server.properties
  sed -i -e "s/^com.openexchange.servlet.maxBodySize=.*/com.openexchange.servlet.maxBodySize=3048576000/" /opt/open-xchange/etc/server.properties

  ##settings management
  sed -i -e "s/JMXBindAddress=.*/JMXBindAddress=${ox_jmxbindaddress}/" /opt/open-xchange/etc/management.properties

  ##settings rmi
  sed -i -e "s/com.openexchange.rmi.host.*/com.openexchange.rmi.host = ${ox_rmi_host}/" /opt/open-xchange/etc/rmi.properties

  ##settings mail
  sed -i -e "s/^com.openexchange.mail.mailStartTls=.*/com.openexchange.mail.mailStartTls=${ox_mail_mailstarttls}/" /opt/open-xchange/etc/mail.properties
  sed -i -e "s/^com.openexchange.mail.transportStartTls=.*/com.openexchange.mail.transportStartTls=${ox_mail_transportstarttls}/" /opt/open-xchange/etc/mail.properties
  sed -i -e "s/^com.openexchange.mail.mailServer=.*/com.openexchange.mail.mailServer=imap:\/\/${ox_mail_server_imap_host}:${ox_mail_server_imap_port}/" /opt/open-xchange/etc/mail.properties
  sed -i -e "s/^com.openexchange.mail.transportServer=.*/com.openexchange.mail.transportServer=smtp:\/\/${ox_mail_server_smtp_host}:${ox_mail_server_smtp_port}/" /opt/open-xchange/etc/mail.properties
  sed -i -e "s/^com.openexchange.mail.loginSource=.*/com.openexchange.mail.loginSource=${ox_mail_server_login_source:-mail}/" /opt/open-xchange/etc/mail.properties
  sed -i -e "s/^com.openexchange.mail.maxForwardCount=.*/com.openexchange.mail.maxForwardCount=100/" /opt/open-xchange/etc/mail.properties

  ##settings imapauth
  #sed -i -e "s/^USE_FULL_LOGIN_INFO_FOR_USER_LOOKUP=.*/USE_FULL_LOGIN_INFO_FOR_USER_LOOKUP=true/" /opt/open-xchange/etc/imapauth.properties
  #sed -i -e "s/^USE_MULTIPLE=.*/USE_MULTIPLE=true/" /opt/open-xchange/etc/imapauth.properties
  #sed -i -e "s/^com.openexchange.imap.maxNumExternalConnections=.*/com.openexchange.imap.maxNumExternalConnections=/" /opt/open-xchange/etc/imapauth.properties

  #settings hunspell.properties
  hunspell_library_path=$(find /usr/ -iname 'libhunspell*' | grep -ve 'doc\|0.0')
  hunspell_share_path="/usr/share/hunspell"
  echo "com.openexchange.spellchecker.hunspell.dictionaries=${hunspell_share_path}" > /opt/open-xchange/etc/hunspell.properties
  echo "com.openexchange.spellchecker.hunspell.library=${hunspell_library_path}" >> /opt/open-xchange/etc/hunspell.properties

  #settings attachment.properties
  
  sed -i -e "s/MAX_UPLOAD_SIZE=.*/MAX_UPLOAD_SIZE=${ox_max_upload_size}/" /opt/open-xchange/etc/attachment.properties

  #settings infostore.properties
  sed -i -e "s/MAX_UPLOAD_SIZE=.*/MAX_UPLOAD_SIZE=${ox_max_upload_size}/"  /opt/open-xchange/etc/infostore.properties

  #mailfilter.properties
  if [ -n "$ox_mail_filter_server_host" ];then
    sed -i -e "s/com.openexchange.mail.filter.server=.*/com.openexchange.mail.filter.server=${ox_mail_filter_server_host:-localhost}/" /opt/open-xchange/etc/mailfilter.properties
    sed -i -e "s/com.openexchange.mail.filter.port=.*/com.openexchange.mail.filter.port=${ox_mail_filter_server_port:-4190}/" /opt/open-xchange/etc/mailfilter.properties
  fi

  sed -i -e "s/com.openexchange.mail.filter.masterPassword=.*/com.openexchange.mail.filter.masterPassword=${ox_mail_filter_master_password:-}/" /opt/open-xchange/etc/mailfilter.properties
  sed -i -e "s/com.openexchange.mail.filter.loginType=.*/com.openexchange.mail.filter.loginType=user/" /opt/open-xchange/etc/mailfilter.properties
  sed -i -e "s/com.openexchange.mail.filter.credentialSource=.*/com.openexchange.mail.filter.credentialSource=imapLogin/" /opt/open-xchange/etc/mailfilter.properties
  
  #/opt/open-xchange/etc/settings/ui.properties 
  sed -i -e "s/\/ui\/mail\/replyTo\/configurable.*/\/ui\/mail\/replyTo\/configurable = true/" /opt/open-xchange/etc/settings/ui.properties 
  sed -i -e "s/\/ui\/global\/expert\/mode\/value.*/\/ui\/global\/expert\/mode\/value = true/" /opt/open-xchange/etc/settings/ui.properties

  #client-onboardin
  if [ "$feature_client_onboarding" == "true" ];then
        sed -i -e "s/^com.openexchange.client.onboarding.enabled=.*/com.openexchange.client.onboarding.enabled=true/" /opt/open-xchange/etc/mail.properties
        sed -i -e "s/^com.openexchange.client.onboarding.syncapp.store.google.playstore=.*/com.openexchange.client.onboarding.syncapp.store.google.playstore=https:\/\/play.google.com\/store\/apps\/details?id=com.openexchange.mobile.syncapp.enterprise/" /opt/open-xchange/etc/client-onboarding-syncapp.properties
        sed -i -e "s/^com.openexchange.client.onboarding.mail.imap.host=.*/com.openexchange.client.onboarding.mail.imap.host=${ox_mail_server_imap_host}/" /opt/open-xchange/etc/client-onboarding-mail.properties
        sed -i -e "s/^com.openexchange.client.onboarding.mail.smtp.host=.*/com.openexchange.client.onboarding.mail.smtp.host=${ox_mail_server_smtp_host}/" /opt/open-xchange/etc/client-onboarding-mail.properties
  fi
  #change user login symbols
  sed -i -e "s/^CHECK_USER_UID_FOR_NOT_ALLOWED_CHARS=.*/CHECK_USER_UID_FOR_NOT_ALLOWED_CHARS=false/" /opt/open-xchange/etc/AdminUser.properties
    

  fix_rights
}


app_firs_start(){
  echo "Try exec funct ${FUNCNAME} --"

  if [[ ! -f "${ox_app_shared_dir_etc}/app_firs_start_done" ]];then
    echo "exec funct ${FUNCNAME} -- initconfigdb"
    /opt/open-xchange/sbin/initconfigdb -i -a \
      --configdb-host="${ox_mysql_db_host}" \
      --configdb-port="${ox_mysql_db_port}"  \
      --configdb-user="${ox_mysql_db_user}" \
      --configdb-pass="${ox_mysql_db_password}"  \
      --mysql-root-user="${ox_mysql_root_user}" \
      --mysql-root-passwd="${ox_mysql_root_db_password}"
    sleep 5;
    echo "exec funct ${FUNCNAME} -- oxinstaller"
    /opt/open-xchange/sbin/oxinstaller -D \
      --no-license  \
      --servername="${ox_servername}" \
      --master-user="${ox_master_user}" \
      --master-pass="${ox_master_pass}" \
      --network-listener-host="${ox_networklistenerhost}" \
      --ajp-bind-port="${ox_ajp_bind_port}" \
      --configdb-user="${ox_mysql_db_user}" \
      --configdb-pass="${ox_mysql_db_password}" \
      --configdb-readhost="${ox_mysql_db_host}" \
      --configdb-writehost="${ox_mysql_db_host}" \
      --configdb-dbname="${ox_config_db_name}" \
      --servermemory="${ox_servermemory}" \
      --imapserver="${ox_mail_server_imap_host}" \
      --smtpserver="${ox_mail_server_smtp_host}"
    sleep 5;

    app_configs_fix

    supervisorctl restart open-xchange;
    echo "sleep 60"
    sleep 60;

    echo "exec funct ${FUNCNAME} -- registerserver"
    /opt/open-xchange/sbin/registerserver --name="${ox_servername}"  --adminuser="${ox_master_user}" --adminpass="${ox_master_pass}"
    sleep 5;
  
    echo "exec funct ${FUNCNAME} -- filestore"
    if [[ ! -d "/var/opt/filestore" ]];then
      mkdir /var/opt/filestore
    fi
    fix_rights
    /opt/open-xchange/sbin/registerfilestore --adminuser="${ox_master_user}" --adminpass="${ox_master_pass}" --storepath="file:/var/opt/filestore" --storesize="${of_filestore_size:-100000}" --maxcontexts=5000
    sleep 5;
    get_filestore_id=$(/opt/open-xchange/sbin/listfilestore --adminuser="${ox_master_user}" --adminpass="${ox_master_pass}" | grep filestore | awk '{ print $1}')
    echo "get_filestore_id = \"$get_filestore_id\""
    sleep 5;
  
    echo "exec funct ${FUNCNAME} -- registerdatabase"
    /opt/open-xchange/sbin/registerdatabase \
      --adminuser="${ox_master_user}" --adminpass="${ox_master_pass}" \
      --dbuser="${ox_mysql_db_user}" --dbpasswd="${ox_mysql_db_password}" --name=oxdatabase_5 \
      --master=true --poolhardlimit=false --maxunit=1000000
    sleep 5;
      
    echo "exec funct ${FUNCNAME} -- update db_pool_url"
    mysql --host="${ox_mysql_db_host}" --port="${ox_mysql_db_port}" --user="${ox_mysql_db_user}" --password="${ox_mysql_db_password}" --database="${ox_config_db_name}" --execute='update db_pool set url = "jdbc:mysql://'$ox_mysql_db_host':'$ox_mysql_db_port'"';
  
    echo "exec funct ${FUNCNAME} -- createcontext"
    /opt/open-xchange/sbin/createcontext \
      --access-combination-name=groupware_standard \
      --adminuser="${ox_master_user}" --adminpass="${ox_master_pass}" \
      --contextid="${ox_context_id}" \
      --quota=-1 \
      --displayname="Context Admin" \
      --givenname="Admin" \
      --surname="User" \
      --username="${ox_oxadminuser_name}" \
      --password="${ox_oxadminuser_password}" \
      --addmapping="${ox_addmapping}" \
      --email="${ox_oxadminuser_email}" \
      --config/com.openexchange.mail.mailServer=imap://${ox_mail_server_imap_host}:${ox_mail_server_imap_port} \
      --config/com.openexchange.mail.transportServer=smtp://${ox_mail_server_smtp_host}:${ox_mail_server_smtp_port} \
      --config/com.openexchange.mail.loginSource=${ox_mail_loginsource} \
      --config/com.openexchange.mail.mailStartTls=${ox_mail_mailstarttls} \
      --config/com.openexchange.mail.transportStartTls=${ox_mail_transportstarttls} \
      --imapserver="imap://${ox_mail_server_imap_host}:${ox_mail_server_imap_port}" \
      --smtpserver="smtp://${ox_mail_server_smtp_host}:${ox_mail_server_smtp_port}"
      sleep 5;
      touch /opt/open-xchange/etc/app_firs_start_done
      if [[ ! -f "${ox_app_shared_dir_etc}/app_firs_start_done" ]];then
        echo "Copy files from /opt/open-xchange/etc/ ${ox_app_shared_dir_etc}/";
        rsync -avP /opt/open-xchange/etc/ ${ox_app_shared_dir_etc}/;
      else
        echo "Error. Skip Copy files from /opt/open-xchange/etc/ ${ox_app_shared_dir_etc}/";
        echo "Error. app_firs_start_done file exist in ${ox_app_shared_dir_etc}";
      fi
      echo "exec funct ${FUNCNAME} -- triggerupdatethemes"
      /opt/open-xchange/sbin/triggerupdatethemes -u
      echo "exec funct ${FUNCNAME} -- initdcsdb"
      /usr/share/open-xchange-documents-collaboration/bin/initdcsdb.sh -a -i \
      --dcsdb-user="${ox_mysql_db_user:-root}" \
      --dcsdb-pass="${ox_mysql_db_password:-yourmysqlrootpassword}" \
      --dcsdb-host="${ox_mysql_db_host:-mysql}" \
      --dcsdb-port="${ox_mysql_db_port:-3306}" \
      --dcsdb-dbname="${dcsdb_db_name:-dcsdb}" \
      --mysql-root-user="${ox_mysql_root_user}" \
      --mysql-root-passwd="${ox_mysql_root_db_password}"

      echo "exec funct ${FUNCNAME} -- documents-collaboration restart"
      supervisorctl restart open-xchange-documents-collaboration
      sleep 5;
      echo "exec funct ${FUNCNAME} -- documents-collaboration status"
      /usr/share/open-xchange-documents-collaboration/bin/documents-collaboration-admin.sh -l


      echo "app_firs_start finished."
  else
    echo "Skip exec funct ${FUNCNAME} --"
    echo "Trigger file ${ox_app_shared_dir_etc}/app_firs_start_done exist";
  fi

}





fix_rights(){
  fix_rights_dirs="/opt/open-xchange/ /var/opt/filestore /persist/open-xchange  /var/log/open-xchange /etc/documents-collaboration /var/spool/open-xchange/uploads /var/www/"
  for fix_rights_dir in $fix_rights_dirs;
  do
    if [[ -d "${fix_rights_dir}" ]];then
      echo "Fix rights to user  open-xchange from user root";echo "Fix rights to group  open-xchange from group root";
      find ${fix_rights_dir}  -user root -exec chown -h -v open-xchange:open-xchange {} +; find ${fix_rights_dir}  -group root -exec chown -h -v open-xchange:open-xchange {} +;
    fi
  done
  chown -R open-xchange:open-xchange /var/opt/filestore
}

supervisord_init_full(){

cat <<OEF > /etc/supervisor/conf.d/open-xchange.conf
[supervisord]
nodaemon=true

[program:open-xchange]
command=su -s /bin/bash -c '. /etc/default/open-xchange && . /opt/open-xchange/etc/ox-scriptconf.sh && /opt/open-xchange/sbin/open-xchange' open-xchange
autostart=true
autorestart=true
stdout_logfile=/dev/stderr
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
#user=open-xchange
stopwaitsecs=35
startsecs = 35
OEF

cat <<OEF > /etc/supervisor/conf.d/open-xchange-documents-collaboration.conf
[supervisord]
nodaemon=true

[program:open-xchange-documents-collaboration]
command=su -s /bin/bash -c '/usr/share/open-xchange-documents-collaboration/bin/com.openexchange.office.dcs -Duser.timezone=UTC --spring.config.location=file:/etc/documents-collaboration/dcs.properties --logging.config=/etc/documents-collaboration/logback-spring.xml' open-xchange
autostart=true
autorestart=true
stdout_logfile=/dev/stderr
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
#user=open-xchange
stopwaitsecs=35
startsecs = 35
OEF

cat <<OEF > /etc/supervisor/conf.d/nginx.conf
[supervisord]
nodaemon=true

[program:nginx]
command=nginx -g 'daemon off;'
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stdout_logfile_maxbytes=0
stderr_logfile_maxbytes=0
autorestart=false
startretries=0
OEF


###cat <<OEF > /etc/supervisor/conf.d/apache2.conf
###[supervisord]
###nodaemon=true
###
###[program:apache2]
###command=/usr/sbin/apache2ctl -DFOREGROUND
###stdout_logfile=/dev/stdout
###stdout_logfile_maxbytes=0
###stderr_logfile=/dev/stderr
###stdout_logfile_maxbytes=0
###stderr_logfile_maxbytes=0
###autorestart=false
###startretries=0
###OEF

cat <<OEF > /etc/supervisor/conf.d/dumb.conf
[supervisord]
nodaemon=true

[program:dumb]
command=tail -f /dev/null
autostart=true
autorestart=true
user=root
startretries=10
priority=1002
stopsignal=TERM
stopwaitsecs=50
stopasgroup=false
killasgroup=false
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0

OEF
}

supervisord_run(){
  if [[ $(${exec_prefix}  ps aux | grep "/usr/bin/supervisord" | grep -ve 'grep') == "" ]];then
          ${exec_prefix} /usr/bin/supervisord -c /etc/supervisor/supervisord.conf
  else
          echo "Process /usr/bin/supervisord. Skip running"
          supervisorctl restart all
  fi

}

main(){



  if [[ "${container_role}" == "worker" ]];then
    app_prepare
    supervisord_init_full
    supervisord_run
  else
    echo "container_role != worker. Only run supervisord"
    supervisord_init_full
    supervisord_run
  fi

  if [[ "$arg_1" == "app_firs_start" ]];then
    app_firs_start
  fi
  if [[ "$arg_1" == "fix_rights" ]];then
    fix_rights
  fi

}

main