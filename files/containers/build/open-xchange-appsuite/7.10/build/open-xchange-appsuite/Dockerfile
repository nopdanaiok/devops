ARG DOCKER_IMAGE_BASE
FROM ${DOCKER_IMAGE_BASE}

ARG APP_VERSION
ARG OX_GID
ARG OX_UID
ARG APP_USER_NAME
ARG APP_USER_ID
ARG APP_GROUP_NAME
ARG APP_GROUP_ID

ENV DEBIAN_FRONTEND=noninteractive
ENV INSTALL_DEPS \
        apt-transport-https \
        gnupg2 \
        software-properties-common \
        wget
ENV CORE_DEPS \
        curl \
        adoptopenjdk-8-hotspot \
        adoptopenjdk-8-hotspot-jre \
        hunspell \
        locales \
        locales-all \
        nano \
        supervisor \
        bash

ENV OX_PACK \
        open-xchange=${APP_VERSION} \
        open-xchange-admin=${APP_VERSION} \
        open-xchange-appsuite=${APP_VERSION} \
        open-xchange-appsuite-backend=${APP_VERSION} \
        open-xchange-appsuite-help-en-us \
        open-xchange-appsuite-l10n-* \
        open-xchange-appsuite-manifest=${APP_VERSION} \
        open-xchange-authentication-database=${APP_VERSION}\
        open-xchange-authorization \
        open-xchange-caldav=${APP_VERSION}\
        open-xchange-carddav=${APP_VERSION}\
        open-xchange-core=${APP_VERSION}\
        open-xchange-dav \
        open-xchange-documentconverter-api \
        open-xchange-documents-backend \
        open-xchange-documents-help-en-us \
        open-xchange-documents-ui \
        open-xchange-documents-ui-static \
        open-xchange-file-distribution=${APP_VERSION}\
        open-xchange-grizzly=${APP_VERSION}\
        open-xchange-halo=${APP_VERSION}\
        open-xchange-hazelcast \
        open-xchange-hazelcast-community \
        open-xchange-mailstore \
        open-xchange-oauth=${APP_VERSION}\
        open-xchange-osgi=${APP_VERSION}\
        open-xchange-passwordchange-database \
        open-xchange-sessionstorage-hazelcast=${APP_VERSION}\
        open-xchange-smtp \
        open-xchange-xerces=${APP_VERSION}\
        open-xchange-l10n-* \
        open-xchange-spamhandler-default \
        open-xchange-pop3 \
        open-xchange-push-dovecot \
        open-xchange-recaptcha \
        open-xchange-spamsettings-generic \
        open-xchange-themes-default \
        open-xchange-websockets-grizzly \
        open-xchange-documents-backend \
        open-xchange-documents-ui \
        open-xchange-documents-ui-static \
        open-xchange-documents-collaboration \
        open-xchange-spellcheck \
        open-xchange-webdav-acl \
        open-xchange-webdav-directory \
        open-xchange-push-mailnotify \
        open-xchange-authorization-standard \
        open-xchange-common \
        open-xchange-configjump-generic \
        open-xchange-mailfilter \
        open-xchange-subscribe \
        open-xchange-resource-managerequest \
        open-xchange-meta-ui-appsuite \
        open-xchange-admin-autocontextid \
        open-xchange-admin-contextrestore \
        open-xchange-admin-oauth-provider \
        open-xchange-admin-plugin-reseller \
        open-xchange-admin-plugin-reseller-soap \
        open-xchange-admin-plugin-user-copy-soap \
        open-xchange-admin-reseller \
        open-xchange-admin-soap \
        open-xchange-admin-soap-reseller \
        open-xchange-admin-soap-usercopy \
        open-xchange-admin-user-copy \
        open-xchange-advertisement \
        open-xchange-antivirus \
        open-xchange-appsuite-spamexperts \
        open-xchange-audit \
        open-xchange-authentication-application-storage-rdb \
        open-xchange-blackwhitelist \
        open-xchange-calendar-printing \
        open-xchange-client-onboarding \
        open-xchange-config-cascade-hostname \
        open-xchange-contact-storage-ldap \
        open-xchange-contacts-ldap \
        open-xchange-datamining \
        open-xchange-dataretention-csv \
        open-xchange-dataretrieval \
        open-xchange-drive \
        open-xchange-drive-comet \
        open-xchange-eas-provisioning-core \
        open-xchange-eas-provisioning-mail \
        open-xchange-eas-provisioning-sms \
        open-xchange-events \
        open-xchange-events-json \
        open-xchange-file-storage-boxcom \
        open-xchange-file-storage-dropbox \
        open-xchange-file-storage-googledrive \
        open-xchange-file-storage-mail \
        open-xchange-file-storage-onedrive \
        open-xchange-file-storage-webdav \
        open-xchange-filestore-s3 \
        open-xchange-filestore-sproxyd \
        open-xchange-gdpr-dataexport \
        open-xchange-geoip \
        open-xchange-geoip-maxmind \
        open-xchange-group-managerequest \
        open-xchange-gui-help-plugin \
        open-xchange-guidedtours \
        open-xchange-hostname-config-cascade \
        open-xchange-linkedin \
        open-xchange-mail-categories \
        open-xchange-messaging \
        open-xchange-messaging-sms \
        open-xchange-meta-appsuite-push \
        open-xchange-meta-databaseonly \
        open-xchange-meta-messaging \
        open-xchange-meta-pubsub \
        open-xchange-meta-ui-appsuite \
        open-xchange-rest \
        open-xchange-sms-sipgate \
        open-xchange-spamhandler-spamassassin \
        open-xchange-soap-cxf \
        open-xchange-sso \
        open-xchange-switchboard \
        open-xchange-unifiedinbox \
        open-xchange-unifiedmail \
        open-xchange-upsell-generic \
        open-xchange-upsell-multiple \
        open-xchange-userfeedback \
        open-xchange-xing-json \
        open-xchange-documents-help-*

ENV LANG en_US.UTF-8

RUN echo "begin" \
    && repo_ver=$(echo "${APP_VERSION}" | awk -F"-" '{ print $1}') \
    && echo "$BUILD_COMMANDS" \
    && echo '' > /etc/apt/sources.list.d/open-xchange-docs.list \
    && apt update \
    && apt install -y $INSTALL_DEPS \
    && echo "Create file /etc/apt/sources.list.d/open-xchange-docs.list" \
        && { \
            echo "deb https://software.open-xchange.com/products/appsuite/${repo_ver}/office/DebianBuster /"; \
            echo "deb https://software.open-xchange.com/products/appsuite/${repo_ver}/office-web/DebianBuster /"; \
            echo "deb https://software.open-xchange.com/products/appsuite/${repo_ver}/documentconverter-api/DebianBuster /"; \
            echo "deb https://software.open-xchange.com/products/appsuite/${repo_ver}/documents-collaboration/DebianBuster /"; \
            echo "deb https://software.open-xchange.com/products/appsuite/${repo_ver}/spellcheck/DebianBuster /"; \
            echo "deb https://software.open-xchange.com/products/appsuite/${repo_ver}/pdftool/DebianBuster /"; \
        } | tee /etc/apt/sources.list.d/open-xchange-docs.list \
    && echo '' > /etc/apt/sources.list.d/open-xchange.list \
    && echo "Create file /etc/apt/sources.list.d/open-xchange.list"  \
       && { \
            echo "deb https://software.open-xchange.com/products/appsuite/${repo_ver}/appsuiteui/DebianBuster/ /"; \
            echo "deb https://software.open-xchange.com/products/appsuite/${repo_ver}//backend/DebianBuster/ /"; \
            echo "deb https://software.open-xchange.com/products/appsuite/${repo_ver}//office/DebianBuster /"; \
            echo "deb https://software.open-xchange.com/products/appsuite/${repo_ver}//office-web/DebianBuster /"; \
            echo "deb https://software.open-xchange.com/products/appsuite/${repo_ver}//documentconverter-api/DebianBuster /"; \
            echo "#deb https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/ Buster main"; \
       } | tee /etc/apt/sources.list.d/open-xchange.list \
    && mkdir -p /usr/share/man/man1  \
    #&& groupadd --gid $APP_GROUP_ID $APP_GROUP_NAME  \
    && useradd --uid $APP_USER_ID --shell /bin/false --home-dir /opt/$APP_USER_NAME --no-create-home $APP_USER_NAME  \
    && usermod -aG  $APP_GROUP_NAME $APP_USER_NAME \
    && wget -q http://software.open-xchange.com/oxbuildkey.pub -O - | apt-key add - \
    && wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add - \
    && add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/ \
    && apt-get update \
    && apt install -y $CORE_DEPS \
    && sed -i -e "s/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/" /etc/locale.gen \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && update-locale LANG=en_US.UTF-8 \
    && apt install -y \
        --no-install-recommends $OX_PACK \
    && echo "Install nginx" \
    && echo "deb http://nginx.org/packages/debian buster nginx" \
    | tee /etc/apt/sources.list.d/nginx.list \
    && curl -fsSL https://nginx.org/keys/nginx_signing.key |  apt-key add - \
    && apt update \
    && apt install nginx -y \
    && rm -rfv /etc/nginx/* /etc/apache2/sites-available/* \
    && mkdir -p /var/run /var/run/nginx  /var/www /var/tmp/nginx/fastcgi /var/lib/nginx/body \
    && chown -R www-data:www-data /var/lib/nginx  /var/tmp/nginx  /var/log/nginx \
    && ox_dirs="/var/spool/open-xchange/uploads /opt/open-xchange /var/www/ /var/log/open-xchange/documents-collaboration" \
    && for ox_dir in ${ox_dirs}; \
        do \
            if [ ! -d "${ox_dir}" ];then mkdir -p "${ox_dir}";fi; \
            chown -R ${APP_USER_NAME}:${APP_GROUP_NAME}  "${ox_dir}"; \
        done \
    &&  if [ ! -f /var/log/open-xchange/documents-collaboration/documents-collaboration.log ];then \
            touch /var/log/open-xchange/documents-collaboration/documents-collaboration.log; \
        fi \
    && log_files_stdout="/var/log/open-xchange/open-xchange-console.log \
                        /var/log/open-xchange/open-xchange.log.0 \
                        /var/log/open-xchange/spellcheck/spellcheck.log \
                        /var/log/open-xchange/open-xchange-console.log.old" \
    && for log_file_stdout in ${log_files_stdout}; \
    do \
            if [ ! -f "$log_file_stdout" ];then touch $log_file_stdout;else echo "File $log_file_stdout exist";fi; \
            #ln -sf /dev/stdout $log_file_stdout; \
    done

ADD ./files/bashrc_alias.sh /usr/sbin/bashrc_alias.sh
ADD ./files/initbash_profile.sh /usr/sbin/initbash_profile
ADD ./files/docker-entrypoint.sh /
ADD ./files/configs/nginx-backend/ /etc/nginx/ 
ADD ./files/configs/supervisord/supervisord.conf /etc/supervisor/supervisord.conf
ADD ./files/configs/apache2/conf-available/proxy_http.conf /etc/apache2/conf-available/proxy_http.conf
ADD ./files/configs/apache2/sites-enabled/open-xchange.conf /etc/apache2/sites-available/open-xchange.conf


RUN sh -x \
    && /bin/bash -C "/usr/sbin/initbash_profile" \
    && chmod +x /docker-entrypoint.sh  \
    && ls -la /etc/apache2/sites-enabled/* \
    && ls -la /etc/apache2/sites-available \
    && rm /etc/apache2/sites-enabled/000-default.conf \
    && a2enmod deflate expires headers lbmethod_byrequests mime proxy proxy_balancer proxy_http rewrite setenvif \
    && a2enconf proxy_http  \
    && apachectl configtest \
    && a2ensite open-xchange
RUN find / -iname 'imapauth.properties'

EXPOSE 80
WORKDIR /var/www/
