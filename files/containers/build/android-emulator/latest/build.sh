#!/usr/bin/env bash
source_dir="/mnt/ssd_1/git/linux2be.com/anroid-emulator-files"
target_dir="/mnt/ssd_1/git/gitlab/devops-f/devops/files/containers/build/android-emulator/latest/build/android-emulator"

if [[ -d "${source_dir}/files" && ! -d "${target_dir}/files" ]];then
    echo "Move for build ${source_dir}/files ${target_dir}/"
    mv ${source_dir}/files ${target_dir}/
fi
#make all
make docker_build
make docker_tag
if [[ ! -d "${source_dir}/files" && -d "${target_dir}/files" ]];then
    echo "Move back ${target_dir}/files ${source_dir}/"
    mv ${target_dir}/files ${source_dir}/
fi
