


registry.gitlab.com/devops-f/devops/vnc-server-xfce:v0.0.1
USER root
apt update --fix-missing && apt install -y supervisor pulseaudio aapt iproute2 socat
mkdir -p /etc/apt/keyrings \
&& curl -fsSL https://swupdate.openvpn.net/repos/repo-public.gpg | gpg --dearmor > /etc/apt/keyrings/openvpn-repo-public.gpg \
&& echo "deb [arch=amd64 signed-by=/etc/apt/keyrings/openvpn-repo-public.gpg] http://build.openvpn.net/debian/openvpn/testing jammy main" > /etc/apt/sources.list.d/openvpn-aptrepo.list \
&& apt update \
&& apt install -y openvpn 



check_system_groups="adm kvm sudo"

for check_system_group in ${check_system_groups};do
    if [ $(getent group ${check_system_group}) ]; then
        echo "group ${check_system_group} exists."
    else
        echo "group ${check_system_group} does not exist. Try create..."
        groupadd -r ${check_system_group}
    fi
done

    for check_system_group in ${check_system_groups};do
        echo "Check user ${DOCKER_IMAGE_RUN_USER:-user} for group ${check_system_group}"
        if [[ ! "$(groups ${DOCKER_IMAGE_RUN_USER:-user})" =~ "${check_system_group}" ]]; then
          echo "Try add user ${DOCKER_IMAGE_RUN_USER:-user} to group ${check_system_group}";       
          usermod -a -G ${check_system_group} ${DOCKER_IMAGE_RUN_USER:-user};
        else
          echo "User ${DOCKER_IMAGE_RUN_USER:-user} in group ${check_system_group}";
        fi
done


RUN mkdir -p /android/sdk /android/apk /home/user && chown -R user:user /android/sdk /android/apk 
USER user
COPY --chown=user ./files/android-1/home/user /home/user
#COPY --chown=user ./files/android-1/android/apk /android/apk
USER root


cp  -rv /git/linux2be.com/anroid-emulator-files/files/android-1/android/apk/* /android/apk/
cp -ra /git/linux2be.com/anroid-emulator-files/files/android-1/home/user/.android  /home/user/
cp -ra /git/linux2be.com/anroid-emulator-files/files/android-1/home/user/Android  /home/user/
```sh
export ANDROID_ADV_NAME="Pixel_3_AP_29"
export DOCKER_IMAGE_RUN_USER=user
export ANDROID_AVD_HOME="${ANDROID_AVD_HOME:-/home/${DOCKER_IMAGE_RUN_USER:-user}/.android/avd}"
export SUPERVISOR_HOME_DIR="/home/${DOCKER_IMAGE_RUN_USER}/supervisor"
export ANDROID_EMULATOR_DIR="${ANDROID_EMULATOR_DIR:-/home/${DOCKER_IMAGE_RUN_USER:-user}/Android/Sdk/emulator}"
export ANDROID_EMULATOR_ARGS="${ANDROID_EMULATOR_ARGS:--avd ${ANDROID_ADV_NAME} -verbose -grpc 8554 -skip-adb-auth  -no-snapshot -feature AllowSnapshotMigration -gpu swiftshader_indirect}"
export ANDROID_ADB_FILE="${ANDROID_ADB_FILE:-/home/${DOCKER_IMAGE_RUN_USER}/Android/Sdk/platform-tools/adb}"

mkdir -p /home/${DOCKER_IMAGE_RUN_USER}/supervisor/{run,log,conf.d}

cat <<OEF> /home/${DOCKER_IMAGE_RUN_USER}/supervisor/supervisord.conf

[unix_http_server]
file=/home/${DOCKER_IMAGE_RUN_USER}/supervisor/run/supervisor.sock
chmod=0770 

[supervisord]
logfile=/home/${DOCKER_IMAGE_RUN_USER}/supervisor/log/supervisord.log
pidfile=/home/${DOCKER_IMAGE_RUN_USER}/supervisor/run/supervisord.pid 
childlogdir=/home/${DOCKER_IMAGE_RUN_USER}/supervisor
user=${DOCKER_IMAGE_RUN_USER}

[inet_http_server]
port=${SUPERVISOR_SERVER_LISTEN_ADDR:-0.0.0.0:9001}

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

[supervisorctl]
serverurl=unix:///home/${DOCKER_IMAGE_RUN_USER}/supervisor/run/supervisor.sock ; use a unix:// URL  for a unix socket

[include]
files = /home/${DOCKER_IMAGE_RUN_USER}/supervisor/conf.d/*.conf
OEF
chown user:user -R /home/${DOCKER_IMAGE_RUN_USER}/supervisor


chown user:user -R /home/${DOCKER_IMAGE_RUN_USER}

if su -s /bin/bash -c "cd ${ANDROID_EMULATOR_DIR} && ./emulator -list-avds" "${DOCKER_IMAGE_RUN_USER:-user}" | grep ${ANDROID_ADV_NAME}
then
    cat <<OEF> ${SUPERVISOR_HOME_DIR}/conf.d/emulator_${ANDROID_ADV_NAME}.conf
[supervisord]
nodaemon=true

[program:emulator_${ANDROID_ADV_NAME}]
environment=DISPLAY=":1",HOME="${HOME}",USER="${DOCKER_IMAGE_RUN_USER:-user}"
priority=999
directory=${ANDROID_EMULATOR_DIR}
command=/home/user/run_${ANDROID_ADV_NAME}.sh
startwaitsecs=15
stopwaitsecs=15
user=${DOCKER_IMAGE_RUN_USER:-user}
stderr_logFile=${SUPERVISOR_HOME_DIR}/log/emulator_${ANDROID_ADV_NAME}.err
stdout_logFile=${SUPERVISOR_HOME_DIR}/log/emulator_${ANDROID_ADV_NAME}.log
OEF
else
    echo "Warning. Skip run emulator_${ANDROID_ADV_NAME} via supervisor. ANDROID_ADV_NAME=${ANDROID_ADV_NAME}  Unknown AVD name."
fi

cat <<OEF> /home/${DOCKER_IMAGE_RUN_USER:-user}/run_${ANDROID_ADV_NAME}.sh
#!/usr/bin/env bash
export DISPLAY=":1"
export HOME="${HOME}"
${ANDROID_EMULATOR_DIR}/emulator ${ANDROID_EMULATOR_ARGS}
OEF


exec  supervisord --nodaemon --configuration /home/${DOCKER_IMAGE_RUN_USER}/supervisor/supervisord.conf

```