#!/usr/bin/env bash
set -euo pipefail

env

# Check environment
if [ -n "${GET_REMOTE_HOST}" ]; then
  echo "GET_REMOTE_HOST script was supplied evaluating, and overriding REMOTE_HOST variable" 
  REMOTE_HOST=$(eval ${GET_REMOTE_HOST})
fi

if [ -z "${REMOTE_HOST}" ]; then
  echo "REMOTE_HOST was not set"
fi

if [ -z "${REMOTE_USER}" ]; then
  echo "REMOTE_USER was not set"
fi

if [ -z "${REMOTE_PASSWORD}" ]; then
  echo "REMOTE_PASSWORD was not set"
fi

printf "Exec proxy-login-automator -local_port ${LOCAL_PORT:-8080} -local_host ${LOCAL_HOST:-0.0.0.0} -remote_host ${REMOTE_HOST} -remote_port ${REMOTE_PORT} -usr ${REMOTE_USER} -pwd ${REMOTE_PASSWORD} -is_remote_https ${REMOTE_HTTPS} -ignore_https_cert ${IGNORE_CERT}\n"
proxy-login-automator -local_port ${LOCAL_PORT:-8080} -local_host ${LOCAL_HOST:-0.0.0.0} -remote_host ${REMOTE_HOST} -remote_port ${REMOTE_PORT} -usr ${REMOTE_USER} -pwd ${REMOTE_PASSWORD} -is_remote_https ${REMOTE_HTTPS} -ignore_https_cert ${IGNORE_CERT}