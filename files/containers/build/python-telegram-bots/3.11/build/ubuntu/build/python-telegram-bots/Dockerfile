ARG BASE_IMAGE
FROM ${BASE_IMAGE}
ARG PIP_REQUIREMENTS
ENV PIP_REQUIREMENTS $PIP_REQUIREMENTS
RUN echo $$PIP_REQUIREMENTS
ENV DEBIAN_FRONTEND noninteractive
ENV runtimeDeps \
        rsync \
        openssh-client \
        bash \
        openssl \
        git \
        curl \
        gettext \
        postgresql-client \
        fping \
        sshpass \
        ca-certificates \
        supervisor \
        screen
ENV buildDeps \
        gcc \
        make \
        g++ \
        cargo \
        python3-dev \
        libffi-dev \
        libpq-dev \
        build-essential \
        zlib1g-dev \
        libncurses5-dev \
        libgdbm-dev \
        libnss3-dev \
        libssl-dev \
        libreadline-dev \
        libffi-dev \
        libsqlite3-dev \
        wget \
        libbz2-dev
ENV PIP_REQUIREMENTS \
        markdown2 \
        livereload==2.6.3 \
        redis==4.5.5 \
        rejson \
        aiodns \
        pika \
        cffi \
        python-dateutil>=2.8.0 \
        arrow>=0.14.3 \
        jmespath \
        pyyaml \
        pycrypto \
        pywinrm \
        jmespath \
        pyapi-gitlab \
        psycopg2-binary==2.9.6 \
        netaddr==0.8.0 \
        demjson3==3.0.6 \
        tornado>=6.0.3 \
        requests>=2.18.4 \
        jsontree>=0.5.1 \
        threaded \
        flask \
        Flask-APScheduler \
        Flask-HTTPAuth \
        gunicorn \
        blinker \
        memorize \
        faust-cchardet \
        python-telegram-bot==20.3 \
        python-gitlab==3.15.0 \
        termcolor \
        PyTelegramBotAPI==4.12.0
RUN apt update --fix-missing && apt install -y  $runtimeDeps \
    && apt install -y $buildDeps \
    && curl -L  https://www.python.org/ftp/python/3.11.4/Python-3.11.4.tgz  -o /tmp/Python-3.11.4.tgz \
    && cd /tmp \
    && tar -xf Python-3.11.*.tgz \
    && cd Python-3.11.*/ \
    && ./configure --enable-optimizations \
    && make -j 8 \
    && make altinstall \
    && python3.11 --version \
    && apt install python3-pip python3-dev -y \
    && curl -sS https://bootstrap.pypa.io/get-pip.py | python3.11 \
    && python3.11 -m pip install install ${PIP_REQUIREMENTS} \
 	&& apt-get purge -y --auto-remove $buildDeps \
 	&& rm -rfv /var/lib/apt/lists/* /tmp/* /root/.cache
ARG RUN_USER