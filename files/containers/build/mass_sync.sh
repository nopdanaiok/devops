#!/usr/bin/env bash 

for d_dir in $(find . -mindepth 2 -maxdepth 5 -iname docker-compose.* -type f | awk  'BEGIN{FS=OFS="/"}{NF--; print }')
do
    ln -sfr Makefile ${d_dir}/Makefile
done
