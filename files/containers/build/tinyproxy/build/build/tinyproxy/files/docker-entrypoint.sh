#!/bin/sh
set -eu; \
    export CONFIG='/etc/tinyproxy/tinyproxy.conf';
    if [ ! -f "$CONFIG"  ]; then
        cp /etc/tinyproxy/tinyproxy.default.conf "$CONFIG";
        ([ -z "$DISABLE_VIA_HEADER" ] || sed -i "s|^#DisableViaHeader .*|DisableViaHeader Yes|" "$CONFIG");
        ([ -z "$STAT_HOST" ]          || sed -i "s|^#StatHost .*|StatHost \"${STAT_HOST}\"|" "$CONFIG");
        ([ -z "$MIN_SPARE_SERVERS" ]  || sed -i "s|^MinSpareServers .*|MinSpareServers $MIN_SPARE_SERVERS|" "$CONFIG");
        ([ -z "$MIN_SPARE_SERVERS" ]  || sed -i "s|^StartServers .*|StartServers $MIN_SPARE_SERVERS|" "$CONFIG");
        ([ -z "$MAX_SPARE_SERVERS" ]  || sed -i "s|^MaxSpareServers .*|MaxSpareServers $MAX_SPARE_SERVERS|" "$CONFIG");
        ([ -z "$ALLOWED_NETWORKS" ]   || for network in $ALLOWED_NETWORKS; do echo "Allow $network" >> "$CONFIG"; done);
        sed -i 's|^LogFile |# LogFile |' "$CONFIG";
    fi;
echo "=============tinyproxy.conf============="
cat /etc/tinyproxy/tinyproxy.conf | grep -vE "^\s*(#|$)"
echo "=============tinyproxy.conf============="
exec /usr/bin/tinyproxy -d;