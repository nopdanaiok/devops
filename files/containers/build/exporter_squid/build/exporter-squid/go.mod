module github.com/boynux/squid-exporter

go 1.13

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/pires/go-proxyproto v0.6.1 // indirect
	github.com/prometheus/client_golang v1.11.0
	github.com/prometheus/common v0.31.1
	github.com/prometheus/procfs v0.7.3 // indirect
	golang.org/x/sys v0.0.0-20211015200801-69063c4bb744 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
