#!/usr/bin/env bash


if [[ "${SERVICE_DISCOVERY_ENABLED}" == "*rue" ]];then
	if [[ -f "/opt/public_libs/files/libs.d/apps/consul.sh" ]];then
		. /opt/public_libs/files/libs.d/apps/consul.sh;
		consul_register_exporter;
	fi
else
	echo "SERVICE_DISCOVERY_ENABLED =! true Skip consul_register_exporter"
fi
echo ""
exec "$@"