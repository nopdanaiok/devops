# nginx deploy kubernetes

kubectl label nodes k8s2 app=backend


openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout tls.key -out tls.crt -subj "/CN=k8s4.cluster.local/O=k8s4.cluster.local"


kubectl create secret tls tls-secret --key tls.key --cert tls.crt




kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.2.0/deploy/static/provider/cloud/deploy.yaml

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.2.0/deploy/static/provider/baremetal/deploy.yaml



kubectl get pods --all-namespaces -l app.kubernetes.io/name=ingress-nginx


wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.2.0/deploy/static/provider/baremetal/deploy.yaml
