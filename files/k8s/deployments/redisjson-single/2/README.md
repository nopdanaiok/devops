# Redisjso deploy k8s - single node

```sh
export k8s_worker_nodes="cl-vm-53-linux2be-local-192-168-122-53 cl-vm-52-linux2be-local-192-168-122-52"
export k8s_app_selector_key="r-js-1"
export k8s_app_selector_value="enabled"
export k8s_label_add="${k8s_app_selector_key}=${k8s_app_selector_value}"
export k8s_app_name="${k8s_app_selector_key}-1"
export k8s_app_namespace="default"
export k8s_app_docker_image_1="registry.gitlab.com/devops_containers/dockers/rejson:2.4.3"
export k8s_app_docker_image_init="registry.gitlab.com/devops_containers/dockers/busybox:latest"
export k8s_app_option_1="true"
export k8s_app_service_port_1="6379"
export deploy_file="/tmp/k8s-deploy-${k8s_app_name}.yaml"

for k8s_worker_node in ${k8s_worker_nodes};do
  kubectl label node ${k8s_worker_node} ${k8s_label_add}
done

cat <<OEF> ${deploy_file}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: ${k8s_app_name}-config
  namespace: ${k8s_app_namespace:-default}
data:
  ALLOW_EMPTY_PASSWORD: "${k8s_app_option_1}"

---
apiVersion: v1
kind: Service
metadata:
  name: svc-${k8s_app_name}
  namespace: ${k8s_app_namespace:-default}
spec:
  type: ClusterIP
  ports:
  - name: ${k8s_app_name}-${k8s_app_service_port_1}
    protocol: TCP
    port: ${k8s_app_service_port_1}
    targetPort: ${k8s_app_service_port_1}
  selector:
    ${k8s_app_selector_key}: ${k8s_app_selector_value}

---
apiVersion: apps/v1 
kind: StatefulSet
metadata:
  name: ${k8s_app_name}
  namespace: ${k8s_app_namespace:-default}
  labels:
    ${k8s_app_selector_key}: "${k8s_app_selector_value}"
spec:
  replicas: 1
  updateStrategy:
    type: RollingUpdate  
  selector:
    matchLabels:
      ${k8s_app_selector_key}: "${k8s_app_selector_value}"
  template:
    metadata:
      labels:
        ${k8s_app_selector_key}: "${k8s_app_selector_value}"
    spec:
      securityContext:
        runAsUser: 1001
        runAsGroup: 1001
        fsGroup: 1001
      restartPolicy: Always
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: ${k8s_app_selector_key}
                operator: In
                values:
                - ${k8s_app_selector_value}
      containers:
      - name: ${k8s_app_name}
        image: ${k8s_app_docker_image_1}
        imagePullPolicy: IfNotPresent
        securityContext:
          allowPrivilegeEscalation: false
        envFrom:
        - configMapRef:
            name: ${k8s_app_name}-config
        ports:
          - name: ${k8s_app_name}-${k8s_app_service_port_1}
            containerPort: ${k8s_app_service_port_1}
      #initContainers:
      #- name: change-ownership-container
      #  image: ${k8s_app_docker_image_init}
      #  command: ["/bin/chown","-R","1001:1001", "/bitnami/postgresql"]
      #  imagePullPolicy: IfNotPresent
      #  securityContext:
      #    runAsUser: 0
      #    privileged: false
      #  volumeMounts:
      #    - mountPath: "${k8s_app_data_conrainer_1}"
      #      name: "${k8s_app_name}"


OEF

kubectl apply -f ${deploy_file}


```