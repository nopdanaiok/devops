# OpenSearch

## 01. Create sc with nfs provisioning

## 02. Create  PersistentVolumeClaim

### 02.1 NFS prov

```sh

cat <<OEF > 01-sc.yaml
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: opensearch-bitnami-master-1-claim-1
spec:
  storageClassName: nfs-client
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 40Gi
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: opensearch-bitnami-master-1-claim-1
spec:
  storageClassName: nfs-client
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 40Gi
      
OEF

k apply -f 01-sc.yaml

```



```sh
cat <<OEF > /tmp/opensearch-cluster-master-svc.yaml
---
apiVersion: v1
kind: Service
metadata:
  name: opensearch-cluster-master
spec:
  type: ClusterIP # 
  externalIPs:
    - 192.168.1.42
OEF
```

```sh
cat <<'OEF'> /tmp/opensearch-master-1-value.yml

antiAffinityTopologyKey: "kubernetes.io/hostname"
antiAffinity: "soft"
###

clusterName: "opensearch-cluster"
nodeGroup: "master"
masterService: "opensearch-cluster-master"
singleNode: false
roles:
  - master
replicas: 1
####
image:
  repository: "opensearchproject/opensearch"
  tag: "2.4.0"
  pullPolicy: "IfNotPresent"

labels: 
  app/opensearch-master-1: "enabled"

nodeSelector: 
   app/opensearch-master-1: "enabled"
#####
sysctlVmMaxMapCount: 262144
terminationGracePeriod: 20
opensearchJavaOpts: "-Xmx2048M -Xms2048M"

persistence:
  enabled: true
  enableInitChown: true
  labels:
    enabled: false
  storageClass: "nfs-client"
  accessModes:
    - ReadWriteOnce
  size: 20Gi
  annotations: {}

OEF
cat <<'OEF'> /tmp/opensearch-data-1-value.yml

antiAffinityTopologyKey: "kubernetes.io/hostname"
antiAffinity: "soft"
###

clusterName: "opensearch-cluster"
nodeGroup: "data"
masterService: "opensearch-cluster-master"

roles:
  - ingest
  - data

replicas: 1
####
image:
  repository: "opensearchproject/opensearch"
  tag: "2.4.0"
  pullPolicy: "IfNotPresent"

labels: 
  app/opensearch-data-1: "enabled"

nodeSelector: 
   app/opensearch-data-1: "enabled"
#####
sysctlVmMaxMapCount: 262144
terminationGracePeriod: 20
opensearchJavaOpts: "-Xmx2048M -Xms2048M"

persistence:
  enabled: true
  enableInitChown: true
  labels:
    enabled: false
  storageClass: "nfs-client"
  accessModes:
    - ReadWriteOnce
  size: 20Gi
  annotations: {}

OEF

cat <<'OEF'> /tmp/opensearch-client-1-value.yml

antiAffinityTopologyKey: "kubernetes.io/hostname"
antiAffinity: "soft"
###

clusterName: "opensearch-cluster"
nodeGroup: "client"
masterService: "opensearch-cluster-master"

roles:
  - remote_cluster_client
replicas: 1
####
image:
  repository: "opensearchproject/opensearch"
  tag: "2.4.0"
  pullPolicy: "IfNotPresent"

labels: 
  app/opensearch-client-1: "enabled"

nodeSelector: 
   app/opensearch-client-1: "enabled"
#####
sysctlVmMaxMapCount: 262144
terminationGracePeriod: 20
opensearchJavaOpts: "-Xmx2048M -Xms2048M"

persistence:
  enabled: true
  enableInitChown: true
  labels:
    enabled: false
  storageClass: "nfs-client"
  accessModes:
    - ReadWriteOnce
  size: 10Gi
  annotations: {}

OEF
cat <<'OEF'> /tmp/opensearch-dashboard-1-value.yml

antiAffinityTopologyKey: "kubernetes.io/hostname"
antiAffinity: "soft"
###

opensearchHosts: "https://opensearch-cluster-master:9200"
replicaCount: 1


####
image:
  repository: "opensearchproject/opensearch-dashboards"
  tag: "2.4.0"
  pullPolicy: "IfNotPresent"

labels: 
  app/opensearch-dashboards-1: "enabled"

nodeSelector: 
   app/opensearch-dashboards-1: "enabled"


OEF


```



kubectl label nodes cl-k8s-c2-192-168-1-42-linux2be-local-192-168-1-42 app/opensearch-master-1=enabled
kubectl label nodes cl-k8s-c2-192-168-1-43-linux2be-local-192-168-1-43 app/opensearch-data-1=enabled
kubectl label nodes cl-k8s-c2-192-168-1-44-linux2be-local-192-168-1-44 app/opensearch-client-1=enabled
kubectl label nodes cl-k8s-c2-192-168-1-44-linux2be-local-192-168-1-44 app/opensearch-dashboards-1=enabled

```sh
helm repo add opensearch https://opensearch-project.github.io/helm-charts/
helm repo update

helm install opensearch-master opensearch/opensearch -f  /tmp/opensearch-master-1-value.yml
helm install opensearch-data opensearch/opensearch -f  /tmp/opensearch-data-1-value.yml
helm install opensearch-client opensearch/opensearch -f  /tmp/opensearch-client-1-value.yml
kubectl port-forward opensearch-cluster-master-0 9200
curl -XGET https://localhost:9200 -u 'admin:admin' --insecure


helm upgrade --recreate-pods opensearch-master opensearch/opensearch -f  /tmp/opensearch-master-1-value.yml
helm upgrade --recreate-pods opensearch-data opensearch/opensearch -f  /tmp/opensearch-data-1-value.yml
helm upgrade --recreate-pods  opensearch-client opensearch/opensearch -f  /tmp/opensearch-client-1-value.yml


```

## Dashboard

helm install opensearch-dashboards opensearch/opensearch-dashboards  -f  /tmp/opensearch-dashboard-1-value.yml
kubectl port-forward dashboards-opensearch-dashboards-575ddd7c4b-cc28b 5601
Visit the url http://localhost:5601/ and use username and password as “admin” to play around with OpenSearch Dashboards

## Services


opensearch-cluster-client            ClusterIP      10.101.164.81    <none>                                   9200/TCP,9300/TCP   3m10s
opensearch-cluster-client-headless   ClusterIP      None             <none>                                   9200/TCP,9300/TCP   3m10s
opensearch-cluster-data              ClusterIP      10.111.226.33    <none>                                   9200/TCP,9300/TCP   3m12s
opensearch-cluster-data-headless     ClusterIP      None             <none>                                   9200/TCP,9300/TCP   3m12s
opensearch-cluster-master            ClusterIP      10.110.4.248     <none>                                   9200/TCP,9300/TCP   3m47s
opensearch-cluster-master-headless   ClusterIP      None             <none>                                   9200/TCP,9300/TCP   3m47s


kubectl patch svc opensearch-cluster-master -p '{"spec":{"externalIPs":["192.168.1.42"]}}'
kubectl patch svc opensearch-dashboards -p '{"spec":{"externalIPs":["192.168.1.44"]}}'


## Clean
helm uninstall opensearch-client
helm uninstall opensearch-master
helm uninstall opensearch-data

helm install opensearch-master opensearch/opensearch -f  /tmp/opensearch-master-1-value.yml
helm install opensearch-data opensearch/opensearch -f  /tmp/opensearch-data-1-value.yml
helm install opensearch-client opensearch/opensearch -f  /tmp/opensearch-client-1-value.yml

# MANS


https://github.com/opensearch-project/helm-charts/tree/main/charts/opensearch

https://github.com/opensearch-project/helm-charts/blob/main/README.md
https://github.com/opensearch-project/helm-charts/blob/main/charts/opensearch/values.yaml