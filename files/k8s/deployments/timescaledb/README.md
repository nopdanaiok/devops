# NATS

helm repo add timescale 'https://charts.timescale.com'
helm repo update

helm install <MY_NAME> timescale/timescaledb-single

helm install <MY_NAME> -f <MY_VALUES.yaml> charts/timescaledb-single

