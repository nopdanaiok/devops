# NATS




```sh
helm install mvp-nats nats/nats --set cluster.enabled=true --set maxPayload=16Mb

```



```sh
mkdir -p configs /git/k8s
git clone https://github.com/bitnami/charts.git /git/k8s/charts-bitnami
curl  -L https://raw.githubusercontent.com/bitnami/charts/master/bitnami/nats/values.yaml  -o ./configs/values-nats-production.yaml
helm install \
	--values ./configs/values-nats-production.yaml \
	--repo /git/k8s/charts-bitnami/bitnami/nats \
	--output yaml \
	--dry-run \
	 mvp-nats 

````



## Customize nats

./configs/values-nats-production.yaml


storageClass: "manual-redis"

## Expose nats
kubectl patch svc my-nats -p '{"spec":{"externalIPs":["192.168.1.132"]}}'


## Snippets

/etc/nats-config/nats.conf