# PG ZOLANDO


## install


### add repo for postgres-operator

helm repo add postgres-operator-charts https://opensource.zalando.com/postgres-operator/charts/postgres-operator

### install the postgres-operator

helm install postgres-operator postgres-operator-charts/postgres-operator --namespace=production
kubectl --namespace=production get pods -l "app.kubernetes.io/name=postgres-operator


### add repo for postgres-operator-ui

helm repo add postgres-operator-ui-charts https://opensource.zalando.com/postgres-operator/charts/postgres-operator-ui

### install the postgres-operator-ui

helm install postgres-operator-ui postgres-operator-ui-charts/postgres-operator-ui --namespace=production
kubectl --namespace=production get pods -l "app.kubernetes.io/name=postgres-operator-ui"


### Deploy cluster  - single node

kubectl label nodes vms-xeon-05-192-168-1-145-worker3 tier=database

kubectl apply -f /git/gitlab/k8s-apps/files/deployments/zalan-do/single/deploy-single-1.yml

kubectl apply -f /git/gitlab/k8s-apps/files/deployments/zalan-do/single/sc-single-1.yml



### Delete

kubectl delete -f /git/gitlab/k8s-apps/files/deployments/zalan-do/single/deploy-single-1.yml
kubectl delete -f /git/gitlab/k8s-apps/files/deployments/zalan-do/single/sc-single-1.yml

### expose on port 

kubectl -n production patch svc postgresql-prod-cluster-1 -p '{"spec":{"externalIPs":["10.48.0.145","192.168.1.145"]}}'





### connect

```sh
export PGPASSWORD=$(kubectl get secret -n production zalando.postgresql-prod-cluster-1.credentials.postgresql.acid.zalan.do  -o 'jsonpath={.data.password}' | base64 -d)
export PGSSLMODE=require
psql -U zalando --host 192.168.1.145
```