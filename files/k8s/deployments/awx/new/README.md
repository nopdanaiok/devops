# AWX-new


## 1. Helm install operator

```sh
helm repo add awx-operator https://ansible.github.io/awx-operator/
helm repo update
```
helm search repo awx-operator

awx-operator/awx-operator       0.17.1          0.17.1          A Helm chart for the AWX Operator

Install

```sh
helm install -n awx --create-namespace my-awx-operator awx-operator/awx-operator
```


## 2. Deploy


```sh
cat <<OEF> secret.yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: awx-linux2be-com-postgres-configuration
  namespace: awx
stringData:
  host: "192.168.1.44"
  port: "5432"
  database: awxnew
  username: awxnew
  password: awxnew
  sslmode: "disable"
  type: unmanaged
type: Opaque

---
apiVersion: v1
kind: Secret
metadata:
  name: awx-linux2be-com-admin-password
  namespace: awx
stringData:
  password: "KmsdksdkjjfdfdfdfdfMdsdkkdshhfddfdfkmdfdHhdsds"


---
apiVersion: v1
kind: Secret
metadata:
  name: awx-linux2be-com-secret-key
  namespace: awx
stringData:
  secret_key: "supersecuresecretkeyKmsdksdkjjfdfdfdfdfMdsdkkdshhfddfdfkmdfdHhdsds"

OEF
k apply -f secret.yaml

cat <<EOF > pvc.yaml
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: awx-static-data-claim-1
  namespace: awx
spec:
  storageClassName: nfs-client
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 10Gi
EOF

k apply -f pvc.yaml



cat <<EOF > deployment.yaml
---
apiVersion: awx.ansible.com/v1beta1
kind: AWX
metadata:
  name: awx-linux2be-com
  namespace: awx
spec:
  service_type: NodePort
  nodeport_port: 30080
  hostname: awx.services.linux2be.com
  projects_persistence: true
  projects_storage_access_mode: ReadWriteOnce
  web_extra_volume_mounts: |
    - name: awx-static-data
      mountPath: /var/lib/awx/public
  extra_volumes: |
    - name: awx-static-data
      persistentVolumeClaim:
        claimName: awx-static-data-claim-1
  security_context_settings:
    runAsGroup: 0
    runAsUser: 0
    fsGroup: 0
    fsGroupChangePolicy: OnRootMismatch
EOF
k apply -f deployment.yaml
```

```sh
cat <<OEF> /tmp/awx-new-service.yaml
---
apiVersion: v1
kind: Service
metadata:
  name: awx-linux2be-com-svc
  namespace: awx
spec:
  type: ClusterIP # NodePort,LoadBalancer,ClusterIP,ExternalName
  ports:
  - name: awx-demo-18052
    protocol: TCP
    port: 18052
    targetPort: 8052
  selector:
    app.kubernetes.io/component: awx

OEF

kubectl -n awx patch svc awx-demo-service-custom -p '{"spec":{"externalIPs":["192.168.1.43","192.168.1.44","192.168.1.42"]}}'

```







  
## Get awx secret

```sh

k -n awx get secrets awx-demo-admin-password -o jsonpath='{.data.password}' | base64 --decode
```


## Delete

```sh
kubectl -n awx delete awx awx-demo
```