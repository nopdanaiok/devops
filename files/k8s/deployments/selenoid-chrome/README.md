# selenoid-chrome

Add lables to nodes(example)

```sh
human@human-TUF-Gaming-FA706II-FX706II:/mnt/hdd/downloads/youtube$ k get nodes
NAME                                     STATUS   ROLES           AGE    VERSION
cl-vm-51-linux2be-local-192-168-122-51   Ready    control-plane   166d   v1.26.0
cl-vm-52-linux2be-local-192-168-122-52   Ready    worker          166d   v1.26.0
cl-vm-53-linux2be-local-192-168-122-53   Ready    worker          166d   v1.26.0

```

```sh
kubectl label nodes cl-vm-72-linux2be-local-192-168-1-72 app-selenoid-chrome=enabled
kubectl label nodes cl-vm-73-linux2be-local-192-168-1-73 app-selenoid-chrome=enabled
```

## 01. Create sc with nfs provis (if neccesary)


## 02. Create  PersistentVolumeClaim

```sh
if [[ ! -d "cd /tmp/selenoid-chrome" ]];then mkdir -p cd /tmp/selenoid-chrome;fi
```

```sh
cd  /tmp/selenoid-chrome/selenoid-chrome
cat <<OEF> 01-pv.yaml
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: selenoid-chrome-claim-1
spec:
  storageClassName: nfs-client
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 30Gi
OEF


k apply -f 01-pv.yaml
```


## 03.1 Deploy app

```sh
cat <<OEF> /tmp/selenoid-chrome/02-configmap.yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: selenoid-chrome-config
data:
  TELEGRAM_BOT_TOKEN: "tg"
  TELEGRAM_CHANNEL_ID: "tg"
  SELENIUM_HOST: "127.0.0.1"
  SELENIUM_PORT: "4444"
  SELENIUM_TARGET_TEST_HOST: "https://awx.services.linux2be.com"
  APP_NAME: "selenoid-chrome"
  APP_HTTP_HOST: "awx.services.linux2be.com"
  APP_VERSION: "latest"
  DEBUG_VERBOSE_LEVEL: "1"
  OVERRIDE_VIDEO_OUTPUT_DIR: "/opt/selenoid/video"
  DRIVER_ARGS: "--disable-dev-shm-usage --enable-chrome-logs --append-log --log-level=WARNING --readable-timestamp --disable-gpu"
OEF
k apply -f /tmp/selenoid-chrome/02-configmap.yaml
```



```sh
cat <<OEF> /tmp/selenoid-chrome/04-deployment.yaml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: selenoid-chrome
spec:
  selector:
    matchLabels:
      app-selenoid-chrome: enabled
  replicas: 3
  strategy:
    type: RollingUpdate
    rollingUpdate:
     maxSurge: 1
     maxUnavailable: 0
  template:
    metadata:
      labels:
        app-selenoid-chrome: enabled
    spec:
      volumes:
      - emptyDir:
          medium: Memory
          sizeLimit: 128Mi
        name: cache-volume
      containers:
      - name: selenoid-chrome
        image: 192.168.1.141:5000/devops-f/devops/selenoid-chrome-patched:102.0.1.a
        args: ["-conf", "/etc/selenoid/browsers.json", "-limit", "31", "-timeout", "180s", "-session-delete-timeout", "5s", "-log-output-dir", "/opt/selenoid/logs", "--enable-logging", "--log-level=DEBUG"]
        resources:
          limits:
            memory: "1000Mi"
            cpu: "1500m"
        ports:
          - name: tcp-4444
            containerPort: 4444
        securityContext:
          privileged: true
          runAsUser: 4096
          capabilities:
            add:
            - IPC_LOCK
            - SYS_RESOURCE
        volumeMounts:
        - mountPath: /dev/shm
          name: cache-volume
        envFrom:
          - configMapRef:
              name: selenoid-chrome-config
        #livenessProbe:
        #    initialDelaySeconds: 12
        #    timeoutSeconds: 8
        #    exec:
        #      command:
        #        - python3
        #        - /docker-healtcheck.py
OEF

k apply -f /tmp/selenoid-chrome/04-deployment.yaml


```
## 03.1 Deploy svc

```sh
cat <<OEF> /tmp/selenoid-chrome/03-service.yaml
apiVersion: v1
kind: Service
metadata:
  name: svc-selenoid-chrome-1
spec:
  type: ClusterIP # NodePort,LoadBalancer,ClusterIP,ExternalName
  sessionAffinity: None #ClientIP or None
  sessionAffinityConfig:
    clientIP: 
      timeoutSeconds: 2
  selector:
    app-selenoid-chrome: enabled
  ports:
    - name: tcp-4444
      protocol: TCP
      port: 4444
      targetPort: 4444
OEF

cat <<OEF> /tmp/selenoid-chrome/03-service-headless.yaml
---
apiVersion: v1
kind: Service
metadata:
  name: svc-selenoid-chrome-headless-1
  labels:
    app-selenoid-chrome: enabled
spec:
  type: ClusterIP
  clusterIP: None
  selector:
    app-selenoid-chrome: enabled
  ports:
    - name: tcp-4444
      protocol: TCP
      port: 4444
      targetPort: 4444
OEF
k apply -f /tmp/selenoid-chrome/03-service.yaml
k apply -f /tmp/selenoid-chrome/03-service-headless.yaml
```



```sh
kubectl patch svc svc-selenoid-chrome-1 -p '{"spec":{"externalIPs":["192.168.1.71","192.168.1.72","192.168.1.73"]}}'
```

## 04. Get creds

```sh
export ROOT_USER=$(kubectl get secret --namespace default selenoid-chrome-1 -o jsonpath="{.data.root-user}" | base64 --decode)
export ROOT_PASSWORD=$(kubectl get secret --namespace default selenoid-chrome-1 -o jsonpath="{.data.root-password}" | base64 --decode)
echo $ROOT_USER $ROOT_PASSWORD 
```


## upgrade



## MANS

https://www.windmill.dev/blog/use-selenium-with-windmill