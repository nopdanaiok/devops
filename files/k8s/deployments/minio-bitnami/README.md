# Minio

## 01. Create sc with nfs provis


## 02. Create  PersistentVolumeClaim


```sh
cd /tmp
cat <<OEF> 01-pv.yaml
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: minio-bitnami-claim-1
spec:
  storageClassName: nfs-client
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 30Gi
OEF


k apply -f 01-pv.yaml
```



## 03. Deploy app
```sh
helm install minio-bitnami-1 bitnami/minio \
  --set image.registry=registry.gitlab.com \
  --set image.repository=devops_containers/dockers/minio    \
  --set image.tag=2022.5.26-debian-10-r4 \
  --set volumePermissions.enabled=true \
  --set volumePermissions.image.pullPolicy=IfNotPresent \
  --set storageClass="nfs-client" \
  --set persistence.storageClass="nfs-client" \
  --set persistence.enabled=true \
  --set persistence.existingClaim="minio-bitnami-claim-1" \
  --set persistence.size=30Gi \
  --debug
    
```
## 04. Optional - change svc

kubectl patch svc minio-bitnami-1 -p '{"spec":{"externalIPs":["192.168.1.43","192.168.1.42","192.168.1.41"]}}'

## 04. Get creds
```sh
export ROOT_USER=$(kubectl get secret --namespace default minio-bitnami-1 -o jsonpath="{.data.root-user}" | base64 --decode)
export ROOT_PASSWORD=$(kubectl get secret --namespace default minio-bitnami-1 -o jsonpath="{.data.root-password}" | base64 --decode)
echo $ROOT_USER $ROOT_PASSWORD 
```


## Helm upgrade

```sh
export ROOT_PASSWORD=$(kubectl get secret --namespace "default" minio-1 -o jsonpath="{.data.root-password}" | base64 -d)
helm upgrade minio-1 bitnami/minio 
  --set image.registry=registry.gitlab.com \
  --set image.repository=devops_containers/dockers/minio    \
  --set image.tag=2022.5.26-debian-10-r4 \
  --set volumePermissions.enabled=true \
  --set volumePermissions.image.pullPolicy=IfNotPresent \
  --set storageClass="nfs-client" \
  --set persistence.storageClass="nfs-client" \
  --set persistence.enabled=true \
  --set persistence.existingClaim="minio-claim-1" \
  --set persistence.size=80Gi
--set startupProbe.timeoutSeconds=6 \
--set auth.rootPassword=$ROOT_PASSWORD

```