# Minio

## 01. Create sc with nfs provisioning


## 02. Create  PersistentVolumeClaim

### 02.1 NFS prov

```sh

cat <<OEF > 01-sc.yaml
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: mysql-primary-claim-1
spec:
  storageClassName: nfs-client
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 40Gi
OEF

k apply -f 01-sc.yaml

```

### 02.2  local-persistent prov(prov fast-disks)

```sh
cat <<OEF > 02-pvc.yaml
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: local-mysql-primary-claim-1
spec:
  storageClassName: fast-disks
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 40Gi
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: local-mysql-primary-pv-1
   labels:
    type: local 
spec:
  storageClassName: fast-disks
  capacity:
    storage: 40Gi
  accessModes:
    - ReadWriteOnce
OEF

k apply -f 02-pvc.yaml
```




```sh
cat <<OEF> my-cnf-custom.yaml
apiVersion: v1
data:
  my.cnf: |-
    [mysqld]
    default_authentication_plugin=mysql_native_password
    skip-name-resolve
    explicit_defaults_for_timestamp
    basedir=/opt/bitnami/mysql
    plugin_dir=/opt/bitnami/mysql/lib/plugin
    port=3306
    socket=/opt/bitnami/mysql/tmp/mysql.sock
    datadir=/bitnami/mysql/data
    tmpdir=/opt/bitnami/mysql/tmp
    max_allowed_packet=256M
    bind-address=0.0.0.0
    pid-file=/opt/bitnami/mysql/tmp/mysqld.pid
    log-error=/opt/bitnami/mysql/logs/mysqld.log
    character-set-server=UTF8
    collation-server=utf8_general_ci
    slow_query_log=0
    slow_query_log_file=/opt/bitnami/mysql/logs/mysqld.log
    long_query_time=10.0
    sql_mode=STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION
    [client]
    port=3306
    socket=/opt/bitnami/mysql/tmp/mysql.sock
    default-character-set=UTF8
    plugin_dir=/opt/bitnami/mysql/lib/plugin

    [manager]
    port=3306
    socket=/opt/bitnami/mysql/tmp/mysql.sock
    pid-file=/opt/bitnami/mysql/tmp/mysqld.pid
kind: ConfigMap
metadata:
  annotations:
    meta.helm.sh/release-name: mysql-bitnami-1
    meta.helm.sh/release-namespace: default
  creationTimestamp: "2022-09-19T09:43:45Z"
  labels:
    app.kubernetes.io/component: primary
    app.kubernetes.io/instance: mysql-bitnami-1
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: mysql
    helm.sh/chart: mysql-9.2.0
  name: mysql-bitnami-config-1
  namespace: default
  resourceVersion: "2284591"
  uid: da5d7c69-8d16-4d51-8108-592e6a66216d
OEF
k apply -f my-cnf-custom.yaml
```

```sh
cat <<OEF > 03-svc.yaml
---
apiVersion: v1
kind: Service
metadata:
  name: mysql-bitnami-1
spec:
  type: ClusterIP # 
  externalIPs:
    - 192.168.1.41
    - 192.168.1.42
    - 192.168.1.43
---
apiVersion: v1
kind: Service
metadata:
  name: mysql-bitnami-1-metrics
spec:
  type: ClusterIP # 
  externalIPs:
    - 192.168.1.41
    - 192.168.1.42
    - 192.168.1.43
OEF
```

## 03. Deploy app 

Set label selector(if neccesary)

```sh
kubectl label nodes cl-k8s-c2-192-168-1-44-linux2be-local-192-168-1-44 app/mysql-bitnami-primary-1=enabled
```

Deploy on custom node with our selector with nfs storage

```sh
helm install mysql-bitnami-1 bitnami/mysql \
  --set image.tag=8.0.30-debian-11-r19 \
  --set volumePermissions.enabled=true \
  --set volumePermissions.image.pullPolicy=IfNotPresent \
  --set primary.persistence.storageClass="nfs-client" \
  --set primary.persistence.enabled=true \
  --set primary.persistence.existingClaim="mysql-primary-claim-1" \
  --set primary.nodeSelector.app/mysql-bitnami-primary-1="enabled"\
  --set persistence.size=39Gi \
  --set architecture=standalone \
  --set metrics.enabled=true \
  --set auth.rootPassword=Cbrt32456 \
  --set primary.existingConfigmap=mysql-bitnami-config-1
```

Deploy on custom node with our selector with local storage

```sh
helm install mysql-bitnami-1 bitnami/mysql \
  --set image.tag=8.0.30-debian-11-r19 \
  --set volumePermissions.enabled=true \
  --set volumePermissions.image.pullPolicy=IfNotPresent \
  --set primary.persistence.storageClass="fast-disks" \
  --set primary.persistence.enabled=true \
  --set primary.persistence.existingClaim="local-mysql-primary-claim-1" \
  --set primary.nodeSelector.app/mysql-bitnami-primary-1="enabled"\
  --set persistence.size=39Gi \
  --set architecture=standalone \
  --set metrics.enabled=true \
  --set auth.rootPassword=Cbrt32456 \
  --set primary.existingConfigmap=mysql-bitnami-config-1
```


## 04. Optional - change svc
```sh
k apply -f 03-svc.yaml
```
or

```sh
kubectl patch svc mysql-bitnami-1 -p '{"spec":{"externalIPs":["192.168.1.42","192.168.1.43","192.168.1.44"]}}'
kubectl patch svc mysql-bitnami-1-metrics -p '{"spec":{"externalIPs":["192.168.1.43","192.168.1.42","192.168.1.41"]}}'
```

## 05. Get creds

```sh
export MYSQL_ROOT_PASSWORD=$(kubectl get secret --namespace default mysql-bitnami-1 -o jsonpath="{.data.mysql-root-password}" | base64 -d)
```

Connect

```sh
mysql --host 192.168.1.43 --port 3306 -u root -p $MYSQL_ROOT_PASSWORD 
```

```sh
kubectl run mysql-bitnami-1-client --rm --tty -i --restart='Never' --image  docker.io/bitnami/mysql:8.0.30-debian-11-r19 --namespace default --env MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD --command -- bash
```






## 06. Upgrade


```sh
helm upgrade mysql-bitnami-1 bitnami/mysql \
--reuse-values \
  --set auth.rootPassword=Cbrt32456 \
  --set primary.existingConfigmap=mysql-bitnami-config-1
```





## 07. Snippets

## 08. Errors fix


## 09. Mans

https://github.com/bitnami/charts/tree/master/bitnami/mysql





