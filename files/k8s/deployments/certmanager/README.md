# Certmanager

kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.9.1/cert-manager.yaml

helm repo add jetstack https://charts.jetstack.io
helm repo update

kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.9.1/cert-manager.crds.yaml

helm install \
	cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.9.1 \
  --set cert-manager.namespace=cert-manager \
  --set cert-manager.release-namespace=cert-manager \
  --set installCRDs=true




## Cloud flare wildcard


```sh
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.9.1/cert-manager.yaml


kubectl -n cert-manager get pods
````

```sh

export cloudflare_api_token="294e5c505119f6aab0c6b6bcff652c1a1f4e0"
export cloudflare_api_email="79091813318@ya.ru"
export cloudflare_wildcard_domain="itc-life.ru"
cat <<OEF> secret.yml
---
apiVersion: v1
kind: Secret
metadata:
  name: cloudflare-api-token-secret
  namespace: cert-manager
type: Opaque
stringData:
  api-token: <plain text format of the token>
OEF

kubectl apply -f secret.yml

cat <<OEF> issuer.yml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: le-global-issuer
spec:
  acme:
    email: ${cloudflare_api_email}
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: letsencrypt-key
    solvers:
    - dns01:
        cloudflare:
          email: ${cloudflare_api_email}
          apiTokenSecretRef:
            name: cloudflare-api-token-secret
            key: api-token

OEF
kubectl apply -f issuer.yml

kubectl get clusterIssuers


cat <<OEF> certificate.yml
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: certificate
  namespace: default
spec:
  dnsNames:
    - "*.${cloudflare_wildcard_domain}"
  secretName: example
  issuerRef:
    name: le-global-issuer
    kind: ClusterIssuer
OEF
kubectl apply -f certificate.yml


cat <<OEF> ingress.yml

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: web-ingress
  namespace: default
  annotations:
    ingress.class: "haproxy"
    haproxy.org/ssl-redirect: "true"
    haproxy.org/ssl-redirect-code: "301
spec:
  tls:
    - hosts:
      - "*.${cloudflare_wildcard_domain}"
      secretName: certificate
  rules:
  - host: ${cloudflare_wildcard_domain}
    http:
      paths:
      - path: /
        backend:
          name: web
          port:
            number: 80
OEF

cat <<OEF> ingress-proxy-external.yml
---
kind: Service
apiVersion: v1
metadata:
  name: external-service-itc-life-ru
  namespace: default
spec:
  type: ExternalName
  externalName: itc-life.ru
  ports:
    - port: 443

---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: external-ingress-itc-life
  namespace: default
  annotations:
    ingress.kubernetes.io/preserve-host: "false"
    ingress.kubernetes.io/secure-backends: "true"
    nginx.ingress.kubernetes.io/upstream-vhost: "itc-life.ru"
    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
    nginx.ingress.kubernetes.io/server-snippet: |
      proxy_ssl_name itc-life.ru;
      proxy_ssl_server_name on;
spec:
  ingressClassName: nginx-example
  tls:
    - hosts:
      - "*.${cloudflare_wildcard_domain}"
      secretName: certificate
  rules:
  - host: itc-life.ru
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: external-service-itc-life-ru
            port:
              number: 443
OEF

kubectl apply -f ingress-proxy-external.yml


```