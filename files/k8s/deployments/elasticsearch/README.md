# Elasticsearch

## 01. Create sc with nfs provisioning

## 02. Create  PersistentVolumeClaim

### 02.1 NFS prov

```sh

cat <<OEF > /tmp/PersistentVolumeClaim.yaml
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: elasticsearch-bitnami-simple-1
spec:
  storageClassName: nfs-client
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 80Gi


k apply -f /tmp/PersistentVolumeClaim.yaml

```



## 03. Deploy app 

Set label selector(if neccesary)

```sh
kubectl label nodes cl-k8s-c2-192-168-1-42-linux2be-local-192-168-1-43 app/elasticsearch-bitnami-simple-1=enabled

```

Deploy on custom node with our selector with nfs storage
```sh
cat <<OEF> /tmp/elasticsearch-bitnami-1.yaml
image.tag=7.17.7
sysctlImage.enabled=True
volumePermissions.enabled=true
volumePermissions.image.pullPolicy=IfNotPresent
master.persistence.storageClass="nfs-client"
master.persistence.enabled=true
master.persistence.existingClaim="elasticsearch-bitnami-simple-1"
master.nodeSelector.app/elasticsearch-bitnami-simple-1="enabled"
persistence.size=79Gi
metrics.enabled=true
master.replicaCount=1
primary.existingConfigmap=elasticsearch-bitnami-config

#Kibana
global.kibanaEnabled=true
OEF


```

```sh
helm install elasticsearch-bitnami-1 -f /tmp/elasticsearch-bitnami-1.yaml bitnami/elasticsearch \
  --set image.tag=7.17.7 \
  --set volumePermissions.enabled=true \
  --set volumePermissions.image.pullPolicy=IfNotPresent \
  --set master.persistence.storageClass="nfs-client" \
  --set master.persistence.enabled=true \
  --set master.persistence.existingClaim="elasticsearch-bitnami-master-1-claim-1" \
  --set master.nodeSelector.app/elasticsearch-bitnami-master-1="enabled"\
  --set persistence.size=39Gi \
  --set architecture=standalone \
  --set metrics.enabled=true \
  --set master.replicaCount=1 \
  --set primary.existingConfigmap=elasticsearch-bitnami-config-1
```


## 04. Optional - change svc

```sh
k apply -f 03-svc.yaml
```
or

```sh
kubectl patch svc elasticsearch-bitnami-1 -p '{"spec":{"externalIPs":["192.168.1.42","192.168.1.43","192.168.1.44"]}}'
kubectl patch svc elasticsearch-bitnami-1-metrics -p '{"spec":{"externalIPs":["192.168.1.43","192.168.1.42","192.168.1.41"]}}'
```

## 05. Get creds

```sh
export elasticsearch_ROOT_PASSWORD=$(kubectl get secret --namespace default elasticsearch-bitnami-1 -o jsonpath="{.data.elasticsearch-root-password}" | base64 -d)
```





## 06. Upgrade


```sh
helm upgrade elasticsearch-bitnami-1 bitnami/elasticsearch \
--reuse-values \
  --set auth.rootPassword=Cbrt32456 \
  --set primary.existingConfigmap=elasticsearch-bitnami-config-1
```





## 07. Snippets

## 08. Errors fix

## 10. Restart

```sh
kubectl rollout restart statefulset opensearch-cluster-data 
kubectl rollout restart statefulset opensearch-cluster-master 
kubectl rollout restart statefulset opensearch-cluster-client 

```

## 09. Mans

https://github.com/bitnami/charts/tree/main/bitnami/elasticsearch/#installing-the-chart




bash openvpn_generate.sh staff_Dan_Kli_3  10.50.0.44 openvpn01.services.foxcom.lv 255.255.255.0