# Install via helm

```sh

helm repo add bitnami https://charts.bitnami.com/bitnami
helm upgrade --install redis bitnami/redis --values values.yaml
```

```sh
kubectl run --namespace default DEPLOYMENT-NAME-redis-client --rm --tty -i --restart='Never' \
    --env REDIS_PASSWORD=$REDIS_PASSWORD \--labels="redis-client=true" \
   --image docker.io/bitnami/redis:5.0.5-debian-9-r141 -- bash
```