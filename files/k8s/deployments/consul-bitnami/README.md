# Consul server

## 1. Install consul


helm repo add bitnami https://charts.bitnami.com/bitnami


### 1.1 Config pv
```sh
cd /tmp

cat <<OEF> /tmp/consul-pv.yaml
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: consul-claim-1
spec:
  storageClassName: nfs-client
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 2Gi
OEF

k apply -f /tmp/consul-pv.yaml
```

### 1.2 Config custom config

```sh

cat <<OEF> /tmp/consul-config-custom.json
{
    "advertise_addr" : "{{ GetInterfaceIP \"eth0\" }}",
    "bind_addr": "{{ GetInterfaceIP \"eth0\" }}",
    "client_addr": "0.0.0.0",
    "node_name": "consul-1-0",
    "datacenter": "dc1",
    "domain": "main.service.consul",
    "server_name" : "main.service.consul",
    "data_dir": "/bitnami/consul",
    "pid_file": "/opt/bitnami/consul/tmp/consul.pid",
    "ui_config": {
      "enabled": true
     },
    "server":true,
    "bootstrap": false,
    "skip_leave_on_interrupt": true,
    "acl": {
      "enabled": true,
      "default_policy": "deny",
      "enable_token_persistence": true,
      "down_policy": "extend-cache",
      "tokens": {
        "agent": "dc80c763-5faa-270e-ab07-880156aa073d"
      }
    },
    "addresses": {
        "http": "0.0.0.0",
        "dns":  "0.0.0.0",
        "https": "0.0.0.0",
        "grpc": "0.0.0.0"
    },
    "ports": {
        "http": 8500,
        "dns": 8600,
        "serf_lan": 8301,
        "server": 8400
    },
    "autopilot": {
        "cleanup_dead_servers": true,
        "last_contact_threshold": "1s",
        "max_trailing_logs": 500
    },
    "disable_update_check": true,
    "log_level": "debug"
}

OEF
for PVC in $(kubectl get pvc | grep consul-1 | awk '{print $1}'  ) ;do kubectl delete pvc $PVC ;done
for PV in $(kubectl get pv | grep consul-1 | awk '{print $1}'  ) ;do kubectl delete pv $PV ;done
kubectl delete configmap consul-config || echo "skip"
kubectl create configmap consul-config --from-file=config.json=/tmp/consul-config-custom.json --from-file=consul.json=/dev/null -o yaml  --dry-run=client | kubectl replace -f -

helm install consul-1 bitnami/consul \
  --set image.registry=registry.gitlab.com \
  --set image.repository=devops_containers/dockers/consul \
  --set image.tag=1.13.1-debian-11-r1 \
  --set volumePermissions.enabled=true \
  --set volumePermissions.image.pullPolicy=IfNotPresent \
  --set storageClass="nfs-client" \
  --set persistence.storageClass="nfs-client" \
  --set persistence.enabled=true \
  --set persistence.existingClaim="consul-claim-1" \
  --set persistence.size=1Gi \
  --set replicaCount=1 \
  --set service.ports.http=8500 \
  --set existingConfigmap=consul-config \
  --debug
```
k rollout restart statefulset consul-1
Exec in container

```sh
consul acl bootstrap
export CONSUL_HTTP_TOKEN=dc80c763-5faa-270e-ab07-880156aa073d
mkdir -p /bitnami/consul/acl
cd /bitnami/consul/acl
consul acl policy create \
  -name "agent" \
  -description "This is an policy for agent to access consul" \
  -rules @consul/consul-anonymous-token-policy.hcl 

consul acl policy create -name "agent" \
  -description "This is an policy for agent to access consul" \
  -rules @agent.hcl

consul acl role create -name "agent" -policy-name="agent"

consul acl token list


```

get new

```sh
SecretID:         0f049925-277d-89c7-b316-26be4c877cc6
```

## 2. service k8s  configure

```sh
kubectl patch svc consul-1-headless -p '{"spec":{"externalIPs":["192.168.1.43"]}}'
```

## 3. Configure app

## 4. Upgrade

```sh
helm upgrade consul-1 bitnami/consul   --reuse-values   --debug
```

## 5. Unstall

```sh
helm  uninstall consul-1 
```


## 5. MANS

https://medium.com/swlh/consul-in-kubernetes-pushing-to-production-223506bbe8db

https://github.com/bitnami/charts/tree/master/bitnami/consul/#installing-the-chart

