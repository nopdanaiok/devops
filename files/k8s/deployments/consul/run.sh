export k8s_worker_nodes="cl-vm-52-linux2be-local-192-168-122-52 cl-vm-53-linux2be-local-192-168-122-53"
export k8s_label_add="app-consul=enabled"

export k8s_app_consul_token="afbcabe9-3afa-15dc-98f4-9fa5997c5223"
export k8s_app_consul_data_1="/mnt/nfs/static/volumes/volume-consul-server-data-1"
export k8s_app_consul_docker_image="registry.gitlab.com/devops_containers/dockers/consul:1.14.3"
for k8s_worker_node in ${k8s_worker_nodes};do
  kubectl label node ${k8s_worker_node} ${k8s_label_add}
done

cat <<OEF> /tmp/consul-deploy-single.yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: consul-server
stringData:
  type: managed
type: Opaque

---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: volume-consul-server-data
  labels:
    app-consul: enabled
spec:
  storageClassName: local-storage
  capacity:
    storage: 2Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Retain
  local:
    path: ${k8s_app_consul_data_1}
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: app-consul
          operator: In
          values:
            - enabled
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: consul-server-env
data:
  HOSTNAME: "consul-server"
  CONSUL_HTTP_TOKEN: "${k8s_app_consul_token}"

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: consul-server-configs
data:
  consul-server-config.json: |
    {
    "advertise_addr" : "{{ GetInterfaceIP \"eth0\" }}",
    "bind_addr": "{{ GetInterfaceIP \"eth0\" }}",
    "client_addr": "0.0.0.0",
    "bootstrap": false,
    "server":true,
    "skip_leave_on_interrupt": true,
    "server_name" : "main.service.consul",
    "datacenter":"dc1",
    "acl": {
        "enabled": true,
        "default_policy": "allow",
        "down_policy": "extend-cache",
        "enable_token_persistence": true,
        "tokens": {
          "master": "${k8s_app_consul_token}",
          "agent_recovery": "${k8s_app_consul_token}"
        }
    }, 
    "ui_config": {
        "enabled": true
    },
    "addresses" : {
        "http": "0.0.0.0",
        "dns":  "0.0.0.0",
        "https": "0.0.0.0",
        "grpc": "0.0.0.0"
    },
    "ports": {
        "http": 8500,
        "dns": 8600,
        "serf_lan": 8301,
        "server": 8400
    },
    "autopilot": {
        "cleanup_dead_servers": true,
        "last_contact_threshold": "1s",
        "max_trailing_logs": 500
    },
    "disable_update_check": true,
    "log_level": "error"
    }

  consul-anonymous-token-policy.hcl: |
    service_prefix "" { policy = "deny" }
    service "" { policy = "deny" } 
    key_prefix "" { policy = "deny" } 
    node_prefix "" { policy = "deny" }
    agent_prefix "" { policy = "deny" }
    query_prefix "" { policy = "deny" }
---
apiVersion: v1
kind: Service
metadata:
  name: svc-consul-server
  labels:
    app-consul: enabled
spec:
  type: ClusterIP 
  selector:
    app-consul: enabled
  ports:
    - name: http-8500
      protocol: TCP
      port: 8500
      targetPort: 8500
    - name: http-8300
      protocol: TCP
      port: 8300
      targetPort: 8300
    - name: http-8301
      protocol: TCP
      port: 8301
      targetPort: 8301
    - name: http-8600
      protocol: TCP
      port: 8600
      targetPort: 8600

---
apiVersion: apps/v1 
kind: StatefulSet
metadata:
  name: consul-server
  labels:
    app-consul: enabled
spec:
  replicas: 1
  updateStrategy:
    type: RollingUpdate
  selector:
    matchLabels:
      app-consul: enabled
  template:
    metadata:
      labels:
        app-consul: enabled
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: app-consul
                operator: In
                values:
                  - enabled
      volumes:
        - name: consul-server-configs
          configMap:
            name: consul-server-configs
        - name: volume-consul-server-data
          hostPath:
            path: "${k8s_app_consul_data_1}"
      initContainers:
      - name: fix-the-volume-permission
        image: busybox
        imagePullPolicy: IfNotPresent
        command:
        - sh
        - -c
        - chown -R 1000:1000 /consul/data
        securityContext:
          privileged: true
        volumeMounts:
          - name: volume-consul-server-data
            mountPath: /consul/data
      containers:
      - name: consul-server
        image: ${k8s_app_consul_docker_image}
        imagePullPolicy: IfNotPresent
        command: ["consul"]
        args: ["agent", "-config-file", "/etc/consul/config.json", "-node=node1", "-server", "-bootstrap-expect", "1", "-data-dir", "/consul/data"]
        volumeMounts:
          - name: volume-consul-server-data
            mountPath: /consul/data
          - name: consul-server-configs
            mountPath: /etc/consul/config.json
            subPath: consul-server-config.json
            readOnly: true
          - name: consul-server-configs
            mountPath: /consul/config/acls/consul-anonymous-token-policy.hcl
            subPath: consul-anonymous-token-policy.hcl
            readOnly: true
        envFrom:
          - configMapRef:
              name: consul-server-env
        env:
          - name: PODIP
            valueFrom:
              fieldRef:
                fieldPath: status.podIP
          - name: NAMESPACE
            valueFrom:
              fieldRef:
                fieldPath: metadata.namespace
        ports:
          - name: http-8500
            containerPort: 8500
          - name: http-8301
            containerPort: 8301
      restartPolicy: Always
      schedulerName: default-scheduler
      terminationGracePeriodSeconds: 30
OEF

if [[ $1 == "deploy" ]];then
  kubectl apply -f /tmp/consul-deploy-single.yaml --wait=true
fi

if [[ $1 == "delete" ]];then
  kubectl delete -f /tmp/consul-deploy-single.yaml --wait=true
fi

#kubectl patch service  svc-consul-server  -p '{"spec": {"externalIPs":["192.168.1.53", "192.168.1.52"]}}'