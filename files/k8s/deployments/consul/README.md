# Consul deploy k8s - single node

```sh
export k8s_worker_nodes="cl-vm-52-linux2be-local-192-168-122-52 cl-vm-53-linux2be-local-192-168-122-53"
export k8s_label_add="app-consul=enabled"

export k8s_app_consul_token="55449cbc-6ee1-64b2-fe10-994de5b35c0d"
export k8s_app_consul_data_1="/mnt/nfs/static/volumes/volume-consul-server-data-1"
export k8s_app_consul_docker_image="registry.gitlab.com/devops_containers/dockers/consul:1.14.3"
for k8s_worker_node in ${k8s_worker_nodes};do
  kubectl label node ${k8s_worker_node} ${k8s_label_add}
done

cat <<OEF> /tmp/consul-deploy-single.yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: consul-server
stringData:
  type: managed
type: Opaque

---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: volume-consul-server-data
  labels:
    app-consul: enabled
spec:
  storageClassName: local-storage
  capacity:
    storage: 2Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Retain
  local:
    path: ${k8s_app_consul_data_1}
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: app-consul
          operator: In
          values:
            - enabled
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: consul-server-env
data:
  HOSTNAME: "consul-server"
  CONSUL_HTTP_TOKEN: "${k8s_app_consul_token}"

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: consul-server-configs
data:
  consul-server-config.json: |
    {
    "advertise_addr" : "{{ GetInterfaceIP \"eth0\" }}",
    "bind_addr": "{{ GetInterfaceIP \"eth0\" }}",
    "client_addr": "0.0.0.0",
    "bootstrap": false,
    "server":true,
    "skip_leave_on_interrupt": true,
    "server_name" : "main.service.consul",
    "datacenter":"dc1",
    "acl": {
        "enabled": true,
        "default_policy": "deny",
        "enable_token_persistence": true,
        "down_policy": "extend-cache",
        "tokens": {
          "master": "${k8s_app_consul_token}",
          "agent_recovery": "${k8s_app_consul_token}"
        }
    }, 
    "ui_config": {
        "enabled": true
    },
    "addresses" : {
        "http": "0.0.0.0",
        "dns":  "0.0.0.0",
        "https": "0.0.0.0",
        "grpc": "0.0.0.0"
    },
    "ports": {
        "http": 8500,
        "dns": 8600,
        "serf_lan": 8301,
        "server": 8400
    },
    "autopilot": {
        "cleanup_dead_servers": true,
        "last_contact_threshold": "1s",
        "max_trailing_logs": 500
    },
    "disable_update_check": true,
    "log_level": "error"
    }

  consul-anonymous-token-policy.hcl: |
    service_prefix "" { policy = "deny" }
    service "" { policy = "deny" } 
    key_prefix "" { policy = "deny" } 
    node_prefix "" { policy = "deny" }
    agent_prefix "" { policy = "deny" }
    query_prefix "" { policy = "deny" }
---
apiVersion: v1
kind: Service
metadata:
  name: svc-consul-server
  labels:
    app-consul: enabled
spec:
  type: ClusterIP 
  selector:
    app-consul: enabled
  ports:
    - name: http-8500
      protocol: TCP
      port: 8500
      targetPort: 8500
    - name: http-8300
      protocol: TCP
      port: 8300
      targetPort: 8300
    - name: http-8301
      protocol: TCP
      port: 8301
      targetPort: 8301
    - name: http-8600
      protocol: TCP
      port: 8600
      targetPort: 8600

---
apiVersion: apps/v1 
kind: StatefulSet
metadata:
  name: consul-server
  labels:
    app-consul: enabled
    
spec:
  replicas: 1
  updateStrategy:
    type: RollingUpdate
  selector:
    matchLabels:
      app-consul: enabled
  template:
    metadata:
      labels:
        app-consul: enabled
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: app-consul
                operator: In
                values:
                  - enabled
      volumes:
        - name: consul-server-configs
          configMap:
            name: consul-server-configs
        - name: volume-consul-server-data
          hostPath:
            path: "${k8s_app_consul_data_1}"
      initContainers:
      - name: fix-the-volume-permission
        image: busybox
        imagePullPolicy: IfNotPresent
        command:
        - sh
        - -c
        - chown -R 1000:1000 /consul/data
        securityContext:
          privileged: true
        volumeMounts:
          - name: volume-consul-server-data
            mountPath: /consul/data
      containers:
      - name: consul-server
        image: ${k8s_app_consul_docker_image}
        imagePullPolicy: IfNotPresent
        command: ["consul"]
        args: ["agent", "-config-file", "/etc/consul/config.json", "-node=node1", "-server", "-bootstrap-expect", "1", "-data-dir", "/consul/data"]
        volumeMounts:
          - name: volume-consul-server-data
            mountPath: /consul/data
          - name: consul-server-configs
            mountPath: /etc/consul/config.json
            subPath: consul-server-config.json
            readOnly: true
          - name: consul-server-configs
            mountPath: /consul/config/acls/consul-anonymous-token-policy.hcl
            subPath: consul-anonymous-token-policy.hcl
            readOnly: true
        envFrom:
          - configMapRef:
              name: consul-server-env
        env:
          - name: PODIP
            valueFrom:
              fieldRef:
                fieldPath: status.podIP
          - name: NAMESPACE
            valueFrom:
              fieldRef:
                fieldPath: metadata.namespace
        ports:
          - name: http-8500
            containerPort: 8500
          - name: http-8301
            containerPort: 8301
      restartPolicy: Always
      schedulerName: default-scheduler
      terminationGracePeriodSeconds: 30
OEF
```

Change config /tmp/consul-deploy-single.yaml for first start -remove section

```yaml
,
        "tokens": {
          "agent_recovery": "${k8s_app_consul_token}",
          "master": "${k8s_app_consul_token}",
          "agent": "${k8s_app_consul_token}"
        }
```
Set

```yml
"default_policy": "allow",
"bootstrap": true,
```

### Deploy

```sh
kubectl apply -f /tmp/consul-deploy-single.yaml --wait=true
```


### Login

```sh
k exec -ti consul-server-0 -- bash
```


### Configure consule polices after first start

Policies list
 - services
 - nodes
 - key/value
 - intentions


### Bootstrap acl

Call in shell

```sh
consul acl bootstrap 
```
to generate a master token which is automatically injected to the server. The master token is the secretID, c66e05fc-61f5–546b-d795-e4e879c15036, as shown below. The master token has unrestricted privileges.


```sh
AccessorID:       6c47a4ad-03c2-a608-2ee1-d6fd7ab866f1
SecretID:         afbcabe9-3afa-15dc-98f4-9fa5997c5223
Description:      Bootstrap Token (Global Management)
Local:            false
Create Time:      2023-01-12 13:46:15.693196129 +0000 UTC
Policies:
   00000000-0000-0000-0000-000000000001 - global-management



```

### Create ACL policies and tokens for Consul clients

The Consul ACL system is composed of two main components: ACL policies and ACL tokens.

* ACL policies: a policy is a set of rules grouped together in a logical unit that can be reused and linked with many tokens.

* ACL tokens: a token is associated with one ACL policy and is authorized for resource access.

For configuration management, we shall have two ACL tokens where one can be used to do CRUD operations on keys and the other is for readonly access. Because the permission of a token is determined by the associated policy, we need to create the corresponding policies using the master token as follows.

```sh
export CONSUL_TOKEN_1="c49e2197-3193-1f81-e14d-6c637001c50c"

cd /tmp/
cat <<OEF> /tmp/full_admin_policy.hcl
acl = "write"
key_prefix "kv/"{
  policy = "write"
}

key_prefix "" {
   policy = "write"
}

service_prefix "" { 
  policy = "write" 
}

node_prefix "" { 
  policy = "write" 
}

agent_prefix "" { 
  policy = "write" 
}

query_prefix "" { 
  policy = "write" 
}


intentions "" {
  policy = "write"
}

sessionn_prefix ""{
  policy = "write"
}

OEF

consul acl policy create \
-name "Full-admin-policy" \
-description "Policy for generating tokens with administrative access to keys" \
-rules @full_admin_policy.hcl \
-token "${CONSUL_TOKEN_1}"

consul acl token create -policy-name="Full-admin-policy" -token "${CONSUL_TOKEN_1}"

```

Result

```sh
AccessorID:       a286d8c9-fc7c-acb6-4167-7b5e26e9d8da
SecretID:         f7a83ada-9e3d-11cd-5a66-41ae578e4d78
Description:      
Local:            false
Create Time:      2023-01-12 15:17:17.321159404 +0000 UTC
Policies:
   44d2c435-c793-66fd-c1a4-5381f6f64746 - Full-admin-policy

```


```sh
cat <<OEF> /tmp/key_readonly_policy.hcl
key_prefix "" {
   policy = "read"
}
OEF
consul acl policy create -name "Key-readonly-policy" -description "Policy for generating tokens with readonly access to keys" -rules @key_readonly_policy.hcl -token "${CONSUL_TOKEN_1}"


cat <<OEF> /tmp/service_write_policy.hcl
service "" {
  policy = "write"
  intentions = "write"
}
OEF
consul acl policy create -name "Service-write-policy" -description "Policy for r/w services" -rules @service_write_policy.hcl -token "${CONSUL_TOKEN_1}"


```

With the above policies, we proceed to create the administrative and readonly tokens using the following commands.

### Create administrative token: d112d7de-e018-f933–1118–212762ccde8f

```sh
consul acl token create -description "Agent administrative token" -policy-name "Key-admin-policy" -token "c66e05fc-61f5-546b-d795-e4e879c15036"
```

Result

```sh
AccessorID:   d530158a-ffe7-710c-10a2-89b004edb9a1
SecretID:     d112d7de-e018-f933-1118-212762ccde8f
Description:  Agent key write token
Local:        false
Create Time:  2018-10-24 09:51:48.851258 +0800 CST
Policies:
   9de2f4da-bb9d-3d86-a3e2-97473b2f06e6 - Key-admin-policy
```


### Create readonly token: c5312125-d375-ba3a-18e4–33c5b3b2f28a

```sh
consul acl token create -description "Agent readonly token" -policy-name "Key-readonly-policy" -token "c66e05fc-61f5-546b-d795-e4e879c15036"
```

Result

```sh
AccessorID:   9836d4ba-d82c-b2a6-55a0-fdc480a3d6ba
SecretID:     c5312125-d375-ba3a-18e4-33c5b3b2f28a
Description:  Agent readonly token
Local:        false
Create Time:  2018-10-24 09:52:58.028827 +0800 CST
Policies:
   68c12def-0f01-7ba2-b654-d562418c9194 - Key-readonly-policy
```


### Use tokens to access Consul KV store

With the administrative and readonly tokens, we now can create keys to and read them from the Consul KV store. Because Consul kv cli does not have the -token argument, we use the HTTP requests to interact with the kv store. Consul supports hierarchical KV store and we leverage this to create a session:ttl key under services/api. To be able to create the key, we use the above administrative token d112d7de-e018-f933–1118–212762ccde8f to submit a HTTP PUT request to the Consul server as follows. If nothing goes wrong, you shall see true returned by the Consul server, indicating the request is successful.


```sh
curl --request PUT --header "X-Consul-Token:f7a83ada-9e3d-11cd-5a66-41ae578e4d78" --data @session:ttl http://127.0.0.1:8500/v1/kv/services/api/session\:ttl
curl --request PUT --header "X-Consul-Token:f7a83ada-9e3d-11cd-5a66-41ae578e4d78" --data @session:ttl http://127.0.0.1:8500/v1/kv/nodes/api/session\:ttl
```


If you use the readonly token c5312125-d375-ba3a-18e4–33c5b3b2f28a to create the key, you shall see Permission denied returned by the server. However, you shall be allowed to get the value of the key session:ttl with the readonly token like below. Note that the value of each key in Consul KV is encoded by Base64 by default. With the administrative and readonly tokens, you can dynamically manage your configurations with proper access control. For example, you may set the administrative token in your CI/CD process and the configurations can be updated only when the change has been reviewed.

```sh
curl --header "X-Consul-Token:c5312125-d375-ba3a-18e4-33c5b3b2f28a" http://127.0.0.1:8500/v1/kv/services/api/session\:ttl
[
    {
        "LockIndex": 0,
        "Key": "services/api/session:ttl",
        "Flags": 0,
        "Value": "MTA=",
        "CreateIndex": 78,
        "ModifyIndex": 104
    }
]
```

After init update config. Set master acl token in k8s_app_consul_token value

Change deployment
Add
```yaml
,
        "tokens": {
          "agent_recovery": "${k8s_app_consul_token}",
          "master": "${k8s_app_consul_token}",
          "agent": "${k8s_app_consul_token}"
        }
```
Set

```yml
"default_policy": "deny",
"bootstrap": false,
```

And redploy.


## Ingress consul

```sh


cat <<'OEF'> /tmp/ingress-nginx-consul-ui.yaml
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: consul-1-ui.services.linux2be.local
  annotations:
    nginx.ingress.kubernetes.io/enable-access-log: "true"
    nginx.ingress.kubernetes.io/server-names-hash-max-size: "2056"
    nginx.ingress.kubernetes.io/log-format: json

spec:
  ingressClassName: nginx
  rules:
    - host: consul-1-ui.services.linux2be.local
      http:
        paths:
        - path: /
          pathType: Prefix
          backend:
            service:
              name: svc-consul-server
              port:
                number: 8500

OEF
k apply -f /tmp/ingress-nginx-consul-ui.yaml


```


## Snippets

```sh
export k8s_app_consul_token="01668778-42f8-c596-34cd-1e41be168414"
consul acl policy create -name consul-anonymous-token-policy -rules @consul/config/acls/consul-anonymous-token-policy.hcl -token=${k8s_app_consul_token}
bash-5.1# consul acl token list -token=${k8s_app_consul_token}
AccessorID:       835e23d3-567e-4ec0-4d5f-99306bac9154
SecretID:         01668778-42f8-c596-34cd-1e41be168414
```

Get acl policy

```sh
consul acl policy list -format json -token "${CONSUL_HTTP_TOKEN}"
```

Delete policy 

```sh
consul acl policy  delete -id=863744e3-c756-ce3b-cdad-cb8e4231e072 -token "${CONSUL_TOKEN_1}"
```

Read token

```sh
consul acl token read -id a953c409-5038-35db-993f-e7f3149f36fb
```

consul acl token access explain -id=<token> -resource=key -label=admin/secret


## Mans

https://faun.pub/introduction-to-consul-kv-with-acl-183e4dd7ee1c
https://mpolinowski.github.io/docs/DevOps/Hashicorp/2021-08-13--hashicorp-consul-access-control-lists/2021-08-13/