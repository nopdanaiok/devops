#!/usr/bin/env bash
export DIR_GIT_ROOT=$(pwd | awk -F\files '{ print $1}' )
cd "${DIR_GIT_ROOT}" || (echo "Error change dir to ${DIR_GIT_ROOT}" && exit 1 )
for shellfile in $(find . -type f  ! -iname 'linter_shell.sh' -iname '*.sh' )
do
	echo "Try fix file ${shellfile#./}"
	shellcheck -f diff "${shellfile#./}"
	shellcheck -f diff "${shellfile#./}" | git apply
done

