# local-persistent

```sh
git clone https://github.com/kubernetes-sigs/sig-storage-local-static-provisioner.git /git/k8s/sig-storage-local-static-provisioner
cd /git/k8s/sig-storage-local-static-provisioner

cat <<OEF > storageclass.yaml
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: fast-disks
provisioner: kubernetes.io/no-provisioner
volumeBindingMode: WaitForFirstConsumer
reclaimPolicy: Retain
OEF


kubectl apply -f storageclass.yaml
```


```sh
mkdir -p provisioner/deployment/kubernetes

helm template ./helm/provisioner > ./provisioner/deployment/kubernetes/provisioner_generated.yaml
kubectl create -f ./provisioner/deployment/kubernetes/provisioner_generated.yaml
```