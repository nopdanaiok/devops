# NFS PROVISIONING

## 1. nfs-subdir-external-provisioner

helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \
    --create-namespace \
    --namespace kube-system \
    --set storageClass.name=nfs-client \
    --set nfs.server=10.11.0.110 \
    --set nfs.path=/mnt/ssd_1/nfs/static \
    --set storageClass.defaultClass=true \
    --set storageClass.allowVolumeExpansion=true \
    --set storageClass.reclaimPolicy=Retain \
    --set storageClass.provisionerName=k8s-sigs.io/nfs-subdir-external-provisioner \
    --set storageClass.accessModes=ReadWriteMany \
    --set nfs.volumeName=nfs-subdir-external-provisioner-root

VARS:

storageclasses.storage.k8s.io=nfs-client

kubectl -n kube-system get pod -o wide

01-pvc.yaml
```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: test-claim-1
spec:
  storageClassName: nfs-client
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
```

05-deployment.pvc
```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: deployment-nfs
spec:
  replicas: 1
  selector:
    matchLabels:
      name: deployment-nfs
  template:
    metadata:
      name: deployment-nfs
      labels:
        name: deployment-nfs
    spec:
      nodeSelector:
        "kubernetes.io/os": linux
      containers:
        - name: deployment-nfs
          image: mcr.microsoft.com/oss/nginx/nginx:1.19.5
          command:
            - "/bin/bash"
            - "-c"
            - set -euo pipefail; while true; do echo $(hostname) $(date) >> /mnt/nfs/outfile; sleep 1; done
          volumeMounts:
            - name: nfs-pvc-deployment-nfs
              mountPath: "/mnt/nfs"
      terminationGracePeriodSeconds: 1
      volumes:
        - name: nfs-pvc-deployment-nfs
          persistentVolumeClaim:
            claimName: test-claim-1
```


## 2. NFS STATIC (csi-driver-nfs)

Install

git clone https://github.com/kubernetes-csi/csi-driver-nfs.git
cd csi-driver-nfs
./deploy/install-driver.sh v4.1.0 local


kubectl -n kube-system get pod -o wide -l app=csi-nfs-controller
kubectl -n kube-system get pod -o wide -l app=csi-nfs-node

Uninstall 

git clone https://github.com/kubernetes-csi/csi-driver-nfs.git
cd csi-driver-nfs
git checkout v4.1.0
./deploy/uninstall-driver.sh v4.1.0 local



uuidgen

fsGroup Support

```sh
kubectl delete CSIDriver nfs.csi.k8s.io
cat <<EOF | kubectl create -f -
apiVersion: storage.k8s.io/v1beta1
kind: CSIDriver
metadata:
  name: nfs.csi.k8s.io
spec:
  attachRequired: false
  volumeLifecycleModes:
    - Persistent
  fsGroupPolicy: File
EOF
```