#!/usr/bin/env bash

DIR_HELM_PACKAGES="files/k8s/helm/helm-packages"
DIR_HELM_CHARTS="files/k8s/helm/helm-charts"
echo "Source remote file ${LIBS_SOURCE_SHELL_COMMON:-https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/shell/common.sh}"
source <(curl -L -s ${LIBS_SOURCE_SHELL_COMMON:-https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/shell/common.sh})
shell_colors

for i in "$@"
do
case $i in
    --action=*) # action=package|publish
    HELM_ACTION="${i#*=}"
    shift # past argument=value
    ;; 
    --default)
    DEFAULT=YES
    shift # past argument with no value
    ;;
    *)
      # unknown option
    ;;
esac
done

helm_package () {

mkdir -p ${DIR_HELM_PACKAGES}
while read chart; do
  echo "[PACKAGING CHART $chart to ${DIR_HELM_PACKAGES}]"
  helm package "$chart" --destination ${DIR_HELM_PACKAGES}
done < <(find ${DIR_HELM_CHARTS}/ -mindepth 1 -maxdepth 1 -type d)

}

helm_publish () {

  echo "CI_COMMIT_TAG=$CI_COMMIT_TAG"
  echo "CI_COMMIT_BRANCH=$CI_COMMIT_BRANCH"
  printf "$Green Exec helm repo list" 
  helm repo list | grep ${CI_MERGE_REQUEST_PROJECT_PATH//'/'/-} || helm repo add ${CI_MERGE_REQUEST_PROJECT_PATH//'/'/-} "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/stable"
  printf "$Green Exec helm repo update" 
  helm repo update
  find ${DIR_HELM_PACKAGES} -mindepth 1 -maxdepth 1 -type f -name '*.tgz' -exec sh -c 'basename "$0"' '{}' \; | while read package; do
    CHART_NAME=$(echo $package | sed -e 's/-[0-9]\.[0-9]\.[0-9]\.tgz$//g')
    CHART_VERSION=$(echo $package | sed -e 's/^[a-zA-Z-].*-//g' | sed -e 's/.tgz$//g')
    CHART_EXISTS=$(helm search repo -l ${CI_MERGE_REQUEST_PROJECT_PATH//'/'/-}/${CHART_NAME}  --version ${CHART_VERSION} | { egrep "$REPO_NAME/$CHART_NAME\s"||true; } | { egrep "$CHART_VERSION\s"||true; } | wc -l)
    if [ $CHART_EXISTS = 0 ]; then
      printf "$Green Load chart ${package}"
      cd ${DIR_HELM_PACKAGES}
      curl --request POST --user gitlab-ci-token:$CI_JOB_TOKEN --form "chart=@${package}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/stable/charts"
    else
      printf "$Blue Chart package $package already exists in Helm repo! Skip!"
    fi
done

}

main () {

  if [[ "${HELM_ACTION}" == "helm_package" ]];then
    helm_package
  fi
  if [[ "${HELM_ACTION}" == "helm_publish" ]];then
    export CI_MERGE_REQUEST_PROJECT_PATH=$(curl --header "PRIVATE-TOKEN: $CI_JOB_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}" | jq -r '.path_with_namespace')
    helm_publish
  fi

}

main